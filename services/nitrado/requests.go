package nitrado

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

// Timeout default timeout value
const Timeout = 65

// Request handler
func Request(output interface{}, method string, url string, params map[string]string, body interface{}, headers map[string]string) *utils.ServiceError {
	httpClient := &http.Client{
		Timeout: time.Second * Timeout,
	}

	var requestBody *bytes.Buffer
	var newRequest *http.Request
	var newRequestErr error

	if body != nil {
		jsonBody, jsonErr := json.Marshal(body)

		if jsonErr != nil {
			serviceErr := utils.NewServiceError(jsonErr)
			serviceErr.SetMessage("Could not marshal request struct")
			serviceErr.SetStatus(http.StatusInternalServerError)
			return serviceErr
		}

		requestBody = bytes.NewBuffer(jsonBody)

		newRequest, newRequestErr = http.NewRequest(method, url, requestBody)
	} else {
		newRequest, newRequestErr = http.NewRequest(method, url, nil)
	}

	if newRequestErr != nil {
		serviceErr := utils.NewServiceError(newRequestErr)
		serviceErr.SetMessage("Failed to create request")
		serviceErr.SetStatus(http.StatusInternalServerError)
		return serviceErr
	}

	if params != nil {
		q := newRequest.URL.Query()

		for key, val := range params {
			q.Add(key, val)
		}

		newRequest.URL.RawQuery = q.Encode()
	}

	for k, v := range headers {
		newRequest.Header.Add(k, v)
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		serviceErr := utils.NewServiceError(requestErr)
		serviceErr.SetMessage("Request failed")
		serviceErr.SetStatus(http.StatusFailedDependency)
		return serviceErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		jsonErr := json.NewDecoder(response.Body).Decode(&output)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			serviceErr := utils.NewServiceError(fmt.Errorf(bodyString))
			serviceErr.SetMessage("Bad response from api endpoint")
			serviceErr.SetStatus(http.StatusInternalServerError)
			return serviceErr
		}

		return nil
	default:
		errorResponse := viewmodels.ErrorResponse{}
		jsonErr := json.NewDecoder(response.Body).Decode(&errorResponse)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			serviceErr := utils.NewServiceError(fmt.Errorf(bodyString))
			serviceErr.SetMessage("Bad response from api endpoint")
			serviceErr.SetStatus(response.StatusCode)
			return serviceErr
		}

		serviceErr := utils.NewServiceError(fmt.Errorf(errorResponse.Error))
		serviceErr.SetMessage(errorResponse.Message)
		serviceErr.SetStatus(response.StatusCode)
		return serviceErr
	}
}
