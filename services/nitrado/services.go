package nitrado

import (
	"fmt"
)

// GetServicesResponse struct
type GetServicesResponse struct {
	Message  string    `json:"message"`
	Services []Service `json:"services"`
}

// Service struct
type Service struct {
	ID                    int         `json:"id"`
	LocationID            int         `json:"location_id"`
	Status                string      `json:"status"`
	WebsocketToken        string      `json:"websocket_token"`
	UserID                int         `json:"user_id"`
	Comment               interface{} `json:"comment"`
	AutoExtension         bool        `json:"auto_extension"`
	AutoExtensionDuration int         `json:"auto_extension_duration"`
	Type                  string      `json:"type"`
	TypeHuman             string      `json:"type_human"`
	ManagedrootID         interface{} `json:"managedroot_id"`
	Details               struct {
		Address       string `json:"address"`
		Name          string `json:"name"`
		Game          string `json:"game"`
		PortlistShort string `json:"portlist_short"`
		FolderShort   string `json:"folder_short"`
		Slots         int    `json:"slots"`
	} `json:"details"`
	StartDate    string   `json:"start_date"`
	SuspendDate  string   `json:"suspend_date"`
	DeleteDate   string   `json:"delete_date"`
	SuspendingIn int      `json:"suspending_in"`
	DeletingIn   int      `json:"deleting_in"`
	Username     string   `json:"username"`
	Roles        []string `json:"roles"`
}

// GetServices func
func (n *NitradoRequest) GetServices(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/services", n.Config.NitradoService.BaseURL)

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.AccountName,
	}

	var response GetServicesResponse

	rErr := Request(&response, "GET", url, nil, nil, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Message:  response.Message,
		Services: response.Services,
	}

	return
}
