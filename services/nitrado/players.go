package nitrado

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
)

// GetPlayersResponse struct
type GetPlayersResponse struct {
	Message    string            `json:"message"`
	Players    []Player          `json:"players"`
	Gameserver models.GameServer `json:"-"`
}

// GetPlayersError struct
type GetPlayersError struct {
	Gameserver models.GameServer
	Error      *utils.ServiceError
}

// SearchPlayersRequestBody struct
type SearchPlayersRequestBody struct {
	PlayerName string `json:"player_name"`
}

// SearchPlayersResponse struct
type SearchPlayersResponse struct {
	Message    string            `json:"message"`
	Players    []Player          `json:"players"`
	Gameserver models.GameServer `json:"-"`
}

// Player struct
type Player struct {
	Name       string   `json:"name"`
	ID         string   `json:"id"`
	IDType     string   `json:"id_type"`
	Online     bool     `json:"online"`
	Actions    []string `json:"actions"`
	LastOnline string   `json:"last_online"`
}

// SearchPlayersError struct
type SearchPlayersError struct {
	Gameserver models.GameServer
	Error      *utils.ServiceError
}

// BanPlayerRequestBody struct
type BanPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// BanPlayerResponse struct
type BanPlayerResponse struct {
	Status     string            `json:"status"`
	Message    string            `json:"message"`
	Gameserver models.GameServer `json:"-"`
}

// BanPlayerError struct
type BanPlayerError struct {
	Gameserver models.GameServer
	Error      *utils.ServiceError
}

// UnbanPlayerRequestBody struct
type UnbanPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// UnbanPlayerResponse struct
type UnbanPlayerResponse struct {
	Status     string            `json:"status"`
	Message    string            `json:"message"`
	Gameserver models.GameServer `json:"-"`
}

// UnbanPlayerError struct
type UnbanPlayerError struct {
	Gameserver models.GameServer
	Error      *utils.ServiceError
}

// SearchPlayersByName func
func (n *NitradoRequest) SearchPlayersByName(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/players/%d/search", n.Config.NitradoService.BaseURL, rd.Gameserver.NitradoID)

	body := SearchPlayersRequestBody{
		PlayerName: rd.Player.Name,
	}

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.TokenName,
	}

	online := "false"
	exact := "false"

	if val, ok := params["online"]; ok {
		online = val
	}

	if val, ok := params["exact-match"]; ok {
		exact = val
	}

	queryParams := map[string]string{
		"online":      online,
		"exact_match": exact,
	}

	var response SearchPlayersResponse

	rErr := Request(&response, "POST", url, queryParams, body, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Message:    response.Message,
		Players:    response.Players,
		Gameserver: rd.Gameserver,
	}
	return
}

// BanPlayer func
func (n *NitradoRequest) BanPlayer(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/players/%d/ban", n.Config.NitradoService.BaseURL, rd.Gameserver.NitradoID)

	body := BanPlayerRequestBody{
		PlayerID: rd.Player.ID,
	}

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.TokenName,
	}

	var response BanPlayerResponse

	rErr := Request(&response, "POST", url, nil, body, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Status:     response.Status,
		Message:    response.Message,
		Gameserver: rd.Gameserver,
	}

	return
}

// UnbanPlayer func
func (n *NitradoRequest) UnbanPlayer(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/players/%d/unban", n.Config.NitradoService.BaseURL, rd.Gameserver.NitradoID)

	body := UnbanPlayerRequestBody{
		PlayerID: rd.Player.ID,
	}

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.TokenName,
	}

	var response UnbanPlayerResponse

	rErr := Request(&response, "DELETE", url, nil, body, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Status:     response.Status,
		Message:    response.Message,
		Gameserver: rd.Gameserver,
	}

	return
}

// GetAllPlayers func
func (n *NitradoRequest) GetAllPlayers(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/players/%d", n.Config.NitradoService.BaseURL, rd.Gameserver.NitradoID)

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.TokenName,
	}

	if val, ok := params["cache-control"]; ok {
		headers["Cache-Control"] = val
	}

	online := "false"
	exact := "false"

	if val, ok := params["online"]; ok {
		online = val
	}

	if val, ok := params["exact-match"]; ok {
		exact = val
	}

	queryParams := map[string]string{
		"online":      online,
		"exact_match": exact,
	}

	var response GetPlayersResponse

	rErr := Request(&response, "GET", url, queryParams, nil, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Message:    response.Message,
		Players:    response.Players,
		Gameserver: rd.Gameserver,
	}

	return
}
