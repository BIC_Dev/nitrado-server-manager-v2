package discordapi

import (
	"encoding/json"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
)

// GetRoles func
func (d *Discord) GetRoles(guildID string) ([]*discordgo.Role, *utils.ServiceError) {
	roles, err := d.DG.GuildRoles(guildID)

	if err != nil {
		var discErr DiscordErr
		umErr := json.Unmarshal([]byte(err.Error()), &discErr)

		if umErr != nil {
			return nil, &utils.ServiceError{
				StatusCode: 0,
				Err:        err,
				Message:    "Failed to get guild roles post",
			}
		}

		return nil, &utils.ServiceError{
			StatusCode: discErr.Code,
			Err:        err,
			Message:    discErr.Message,
		}
	}

	return roles, nil
}

// GetChannel func
func (d *Discord) GetChannel(channelID string) (*discordgo.Channel, *utils.ServiceError) {
	channel, err := d.DG.Channel(channelID)

	if err != nil {
		discErr, parseErr := ParseError(err)

		if parseErr != nil {
			return nil, parseErr
		}

		return nil, discErr
	}

	return channel, nil
}
