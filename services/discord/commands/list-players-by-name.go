package commands

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// SearchPlayer struct
type SearchPlayer struct {
	Name         string
	ID           string
	IDType       string
	Online       bool
	Actions      []string
	LastOnline   string
	Servers      []string
	OnlineServer string
}

// SearchPlayerError struct
type SearchPlayerError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

const minLengthSearchPlayerName = 3

// ListPlayersByNameCommand func
func (ch *CommandHandler) ListPlayersByNameCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Search players: %s", message.Content), ch.Log.LogInformation)

	playerName := strings.Join(args, " ")

	if len(playerName) < minLengthSearchPlayerName {
		ch.SendErrorMessage("ERROR: Search Players", "Invalid search", fmt.Sprintf("Must include %d or more characters", minLengthSearchPlayerName), "", message.Content, message.ChannelID)
		return
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: List Players: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: List Players", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: List Players: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: List Players", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "false",
	}

	for _, gameserver := range gameservers {
		if !gameserver.Enabled {
			continue
		}

		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Player:     nitrado.Player{Name: playerName},
			Gameserver: gameserver,
		})
	}

	responses, errors := request.GoRequest(request.SearchPlayersByName, requestData, params)

	// if len(responses) == 0 {
	// 	ch.SendErrorMessage("Search Players", "No players found", "", "", message.Content, message.ChannelID)
	// 	return
	// }

	sp := ParsePlayersFromRequestResponse(responses)

	var embeddablePlayers []EmbeddableField

	for _, player := range sp {
		embeddablePlayers = append(embeddablePlayers, player)
	}

	var spe SearchPlayerError

	for _, err := range errors {
		// Ignore 404 errors
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		spe.Errs = append(spe.Errs, err.Error)
		spe.Gameservers = append(spe.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(spe.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &spe)
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddablePlayers) == 0 {
		description += "\n**No players found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Search Players by Name",
		Description: description,
		Color:       ch.Config.Bot.Commands["player"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddablePlayers, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post player search list: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	return
}

// ConvertToEmbedField func
func (sp *SearchPlayer) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var servers string

	maxChars := 800
	for _, server := range sp.Servers {
		if len(servers) >= maxChars {
			break
		}

		if len(servers)+len(server) >= maxChars {
			break
		}

		servers = servers + "\n\t" + server
	}

	var playerName string
	var fieldVal string

	if sp.Online {
		playerName = "🟢 " + sp.Name
		fieldVal = fmt.Sprintf("```Nitrado ID: %s\nOnline Server: %s\nServers: %s```", sp.ID, sp.OnlineServer, servers)
	} else {
		playerName = "🔴 " + sp.Name
		fieldVal = fmt.Sprintf("```Nitrado ID: %s\nLast Online: %s\nServers: %s```", sp.ID, sp.LastOnline, servers)
	}

	field := &discordgo.MessageEmbedField{
		Name:   playerName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *SearchPlayerError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	servers := ""
	maxChars := 800
	for _, server := range spe.Gameservers {
		if len(servers) >= maxChars {
			break
		}

		if len(servers)+len(server.Name) >= maxChars {
			break
		}

		servers = servers + "\n" + server.Name
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to search gameservers",
		Value:  fmt.Sprintf("```%s```", servers),
		Inline: false,
	}

	return field, nil
}

// ParsePlayersFromRequestResponse func
func ParsePlayersFromRequestResponse(rr []*nitrado.RequestResponse) map[string]*SearchPlayer {
	var sp map[string]*SearchPlayer = make(map[string]*SearchPlayer)

	for _, response := range rr {
		if response.Players == nil {
			continue
		}

		for _, player := range response.Players {
			if _, ok := sp[player.ID]; ok {
				sp[player.ID].Servers = append(sp[player.ID].Servers, response.Gameserver.Name)

				if player.Online {
					sp[player.ID].OnlineServer = response.Gameserver.Name
				}

				if sp[player.ID].LastOnline < player.LastOnline {
					sp[player.ID].LastOnline = player.LastOnline
				}
			} else {
				sp[player.ID] = &SearchPlayer{
					Name:       player.Name,
					ID:         player.ID,
					IDType:     player.IDType,
					Online:     player.Online,
					Actions:    player.Actions,
					LastOnline: player.LastOnline,
					Servers:    []string{response.Gameserver.Name},
				}

				if player.Online {
					sp[player.ID].OnlineServer = response.Gameserver.Name
				}
			}
		}
	}

	return sp
}
