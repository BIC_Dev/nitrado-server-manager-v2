package commands

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// RenameGameserver struct
type RenameGameserver struct {
	OriginalGameServerName string
	NewGameServerName      string
}

// RenameGameserverError struct
type RenameGameserverError struct {
	Error   error
	Message string
}

// RenameGameserverCommand func
func (ch *CommandHandler) RenameGameserverCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Rename Game Server: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 2 {
		ch.SendErrorMessage("ERROR: Rename Game Server", "Please include the server ID and a new name for the server.", "", "", message.Content, message.ChannelID)
		return
	}

	gameServerID, gsIDErr := strconv.Atoi(args[0])
	if gsIDErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get parse game server ID (%s): %s", args[0], gsIDErr.Error()), ch.Log.LogLow)
		ch.SendErrorMessage("ERROR: Rename Game Server", "Please verify your game server ID and retry the command.", "", "", message.Content, message.ChannelID)
		return
	}

	successOutput := RenameGameserver{}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log("COMMAND: Rename Game Servers: Failed to start database", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Rename Game Server", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	guild, gErr := guild.GetLatest(dbInt)
	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Rename Game Servers: Failed to get guild: %s", message.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Rename Game Server", "Failed to find guild set up for bot. Please run setup first before adding command roles.", "", "", message.Content, message.ChannelID)
		return
	}

	gameServer := models.GameServer{
		GuildID:   guild.ID,
		NitradoID: gameServerID,
	}

	gameServer, gsErr := gameServer.GetByGameServerID(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Rename Game Servers: Failed to get game server (%s) for guild (%s): %s", args[0], guild.GuildID, gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Rename Game Server", fmt.Sprintf("Failed to find game server (%s). Please try the command again or run setup.", args[0]), "", "", message.Content, message.ChannelID)
		return
	}

	if gameServer.ID == 0 || !gameServer.Enabled {
		ch.Log.Log(fmt.Sprintf("COMMAND: Rename Game Servers: Failed to get game server (%s) for guild (%s)", args[0], guild.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Rename Game Server", fmt.Sprintf("The game server (%s) does not exist or is disabled.", args[0]), "", "", message.Content, message.ChannelID)
		return
	}

	originalGameServerName := gameServer.Name
	gameServerName := strings.Join(args[1:], " ")
	gameServer.Name = gameServerName

	gsUpErr := gameServer.Update(dbInt)
	if gsUpErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Rename Game Servers: Failed to update game server (%s) for guild (%s): %s", args[0], guild.GuildID, gsUpErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Rename Game Server", fmt.Sprintf("Failed to update game server (%s). Please verify the name for the game server and try the command again.", args[0]), "", "", message.Content, message.ChannelID)
		return
	}

	embedParams := EmbeddableParams{
		Title:       "Rename Game Server",
		Description: fmt.Sprintf("**Renamed Game Server:** `%d`", gameServer.NitradoID),
		Color:       ch.Config.Bot.Commands["nameserver"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	successOutput.OriginalGameServerName = originalGameServerName
	successOutput.NewGameServerName = gameServerName

	var embeddableFields []EmbeddableField
	embeddableFields = append(embeddableFields, &successOutput)

	var embeddableErrors []EmbeddableError

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post add role message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}
}

// ConvertToEmbedField func
func (sp *RenameGameserver) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```Original Name: \n\t%s\n\nNew Name: \n\t%s```", sp.OriginalGameServerName, sp.NewGameServerName)

	field := &discordgo.MessageEmbedField{
		Name:   "Renamed Game Server",
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *RenameGameserverError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "Failed to Rename Game Server",
		Value:  spe.Message,
		Inline: false,
	}

	return field, nil
}
