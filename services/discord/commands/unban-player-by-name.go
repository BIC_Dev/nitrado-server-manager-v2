package commands

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// UnbanPlayer struct
type UnbanPlayer struct {
	PlayerName string
	PlayerID   string
	Servers    []models.GameServer
}

// UnbanPlayerError struct
type UnbanPlayerError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

const minLengthUnbanPlayerName = 3

// UnbanPlayerByNameCommand func
func (ch *CommandHandler) UnbanPlayerByNameCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Unban Player: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 1 {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	var singleUnbanGameserver models.GameServer
	potentialGameserverID, pe := strconv.Atoi(args[0])

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Unban Player: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Unban Player", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Unban Player: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Unban Player", "Failed to get game servers. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	for _, aServer := range gameservers {
		if !aServer.Enabled {
			continue
		}
		if aServer.NitradoID == potentialGameserverID {
			singleUnbanGameserver = aServer
			break
		}
	}

	var playerName string

	if singleUnbanGameserver.ID != 0 {
		if len(args) < 2 {
			ch.SendErrorMessage("ERROR: Unban Player", "Missing player name", fmt.Sprintf("Must include player name with %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
			return
		}

		playerName = strings.Join(args[1:], " ")
	} else if len(args[0]) > 5 && pe == nil {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Config.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
		return
	} else {
		playerName = strings.Join(args, " ")
	}

	if len(playerName) < minLengthUnbanPlayerName {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid search", fmt.Sprintf("Must include %d or more characters", minLengthSearchPlayerName), "", message.Content, message.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "true",
	}

	for _, gameserver := range gameservers {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Player:     nitrado.Player{Name: playerName},
			Gameserver: gameserver,
		})
	}

	responses, errors := request.GoRequest(request.SearchPlayersByName, requestData, params)

	// if len(responses) == 0 {
	// 	ch.SendErrorMessage("Unban Player", "No players found", "", "", message.Content, message.ChannelID)
	// 	return
	// }

	sp := ParsePlayersFromRequestResponse(responses)

	var embeddablePlayers []EmbeddableField

	for _, player := range sp {
		embeddablePlayers = append(embeddablePlayers, player)
	}

	var spe SearchPlayerError

	for _, err := range errors {
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		spe.Errs = append(spe.Errs, err.Error)
		spe.Gameservers = append(spe.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(spe.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &spe)
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddablePlayers) > 0 {
		if singleUnbanGameserver.ID != 0 {
			description += fmt.Sprintf("\nClick on the %s reaction to **unban** the player on %s", ch.Command.Reactions["unban_player"].Icon, singleUnbanGameserver.Name)
		} else {
			description += fmt.Sprintf("\nClick on the %s reaction to **unban** the player", ch.Command.Reactions["unban_player"].Icon)
		}

	} else {
		description += "\n**No player found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Unban Player by Name",
		Description: description,
		Color:       ch.Config.Bot.Commands["unban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddablePlayers, embeddableErrors)

	var lastMessage *discordgo.Message
	for _, embed := range embeds {
		// Can get message ID from response here
		postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post unban player: %s", cpErr.Error()), ch.Log.LogMedium)
			continue
		}

		lastMessage = postedMessage
	}

	if len(embeddablePlayers) == 0 {
		return
	}

	if lastMessage == nil {
		return
	}

	prErr := ch.PostReaction(lastMessage.ChannelID, lastMessage.ID, ch.Command.Reactions["unban_player"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	var mErr *utils.ModelError

	if singleUnbanGameserver.ID != 0 {
		mErr = AddPotentialUnbanToDB(ch.Config, lastMessage.ID, message.Author.ID, sp, singleUnbanGameserver.NitradoID)
	} else {
		mErr = AddPotentialUnbanToDB(ch.Config, lastMessage.ID, message.Author.ID, sp, 0)
	}

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// HandleUnbanReaction func
func (ch *CommandHandler) HandleUnbanReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	unban, gErr := GetPotentialUnbanFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if unban.PlayerID == "" {
		ch.Log.Log("ERROR: No potential unban found", ch.Log.LogInformation)
		return
	}

	if unban.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to unban user", ch.Log.LogInformation)
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Unban Player: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Unban Player", "Failed to start the database. Please try the command again.", "", "", "", reaction.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: reaction.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Unban Player: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Unban Player", "Failed to get game servers. Please try the command again.", "", "", "", reaction.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	for _, gameserver := range gameservers {
		if !gameserver.Enabled {
			continue
		}
		if unban.GameserverID == 0 {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: unban.PlayerID},
				Gameserver: gameserver,
			})
		} else if unban.GameserverID == gameserver.NitradoID {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: unban.PlayerID},
				Gameserver: gameserver,
			})
		}
	}

	finished := false
	start := 0
	max := 10
	var responses []*nitrado.RequestResponse
	var errors []*nitrado.RequestError
	for !finished {
		if start >= len(requestData) {
			finished = true
		} else if start+max > len(requestData) {
			resps, errs := request.GoRequest(request.UnbanPlayer, requestData[start:], nil)
			responses = append(responses, resps...)
			errors = append(errors, errs...)
			finished = true
		} else {
			resps, errs := request.GoRequest(request.UnbanPlayer, requestData[start:start+max], nil)
			responses = append(responses, resps...)
			errors = append(errors, errs...)
			start = start + max
		}
	}

	ub := ParseGameserversFromUnbanRequestResponse(responses)

	var embeddableFields []EmbeddableField

	if ub != nil {
		ub.PlayerID = unban.PlayerID
		ub.PlayerName = unban.PlayerName
		embeddableFields = append(embeddableFields, ub)
	}

	var ube UnbanPlayerError

	for _, err := range errors {
		// Ignore 404 errors
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		ube.Errs = append(ube.Errs, err.Error)
		ube.Gameservers = append(ube.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ube.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ube)
	}

	embedParams := EmbeddableParams{
		Title:       "Unban Player by Name",
		Description: fmt.Sprintf("**Unbanned:** `%s`\nThis will be applied when Nitrado updates Ark with the new unban. It may take some time.", ub.PlayerName),
		Color:       ch.Config.Bot.Commands["unban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(reaction.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post unban player message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	dErr := DeletePotentialUnbanFromDB(ch.Config, unban)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sp *UnbanPlayer) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```Player ID: %s\nServers: %d```", sp.PlayerID, len(sp.Servers))

	field := &discordgo.MessageEmbedField{
		Name:   sp.PlayerName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *UnbanPlayerError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	charCount := 0
	for _, gs := range spe.Gameservers {
		if charCount+len(gs.Name) >= MaxEmbedFieldCharCount {
			break
		}

		gameservers = append(gameservers, gs.Name)
		charCount += len(gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to unban player on gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}

// ParseGameserversFromUnbanRequestResponse func
func ParseGameserversFromUnbanRequestResponse(rr []*nitrado.RequestResponse) *UnbanPlayer {
	var ubp UnbanPlayer

	for _, gs := range rr {
		ubp.Servers = append(ubp.Servers, gs.Gameserver)
	}

	return &ubp
}

// AddPotentialUnbanToDB func
func AddPotentialUnbanToDB(config *utils.Config, messageID string, userID string, players map[string]*SearchPlayer, gameserverID int) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	for _, player := range players {
		potentialBan := &models.PotentialUnban{
			MessageID:     messageID,
			PlayerID:      player.ID,
			PlayerName:    player.Name,
			DiscordUserID: userID,
			GameserverID:  gameserverID,
		}

		mErr := potentialBan.Create(dbInt)

		if mErr != nil {
			return mErr
		}
	}

	return nil
}

// GetPotentialUnbanFromDB func
func GetPotentialUnbanFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialUnban, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialUnban := &models.PotentialUnban{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbBan, mErr := potentialUnban.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbBan, nil
}

// DeletePotentialUnbanFromDB func
func DeletePotentialUnbanFromDB(config *utils.Config, unban *models.PotentialUnban) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := unban.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
