package commands

import (
	"fmt"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// RestartGameserver struct
type RestartGameserver struct {
	Gameserver models.GameServer
}

// RestartGameserverError struct
type RestartGameserverError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

const minLengthRestartGameserverName = 7

// RestartGameserverCommand func
func (ch *CommandHandler) RestartGameserverCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: %s", message.Content), ch.Log.LogInformation)

	if len(args) == 0 || len(args) > 1 {
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Invalid gameserver ID", fmt.Sprintf("Must include %d or more characters", minLengthRestartGameserverName), "", message.Content, message.ChannelID)
		return
	}

	gsID, cvErr := strconv.Atoi(args[0])
	if cvErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Invalid game server ID: %s", cvErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Invalid game server ID", "", "", message.Content, message.ChannelID)
		return
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Failed to get game servers. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	var gameserver models.GameServer
	for _, aServer := range gameservers {
		if !aServer.Enabled {
			continue
		}

		if aServer.NitradoID == gsID {
			gameserver = aServer
			break
		}
	}

	if gameserver.ID == 0 {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Game Server not found: %d", gsID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Game Server not found", "", "", message.Content, message.ChannelID)
		return
	}

	var embeddableGameserver []EmbeddableField

	embeddableGameserver = append(embeddableGameserver, &RestartGameserver{
		Gameserver: gameserver,
	})

	var embeddableErrors []EmbeddableError

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddableGameserver) > 0 {
		description += fmt.Sprintf("\nClick on the %s reaction to **restart** the gameserver", ch.Command.Reactions["restart_gameserver"].Icon)
	} else {
		description += "\n**No gameserver found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Restart Gameserver by ID",
		Description: description,
		Color:       ch.Config.Bot.Commands["restart"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableGameserver, embeddableErrors)

	var lastMessage *discordgo.Message
	for _, embed := range embeds {
		// Can get message ID from response here
		postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post restart gameserver: %s", cpErr.Error()), ch.Log.LogMedium)
			continue
		}

		lastMessage = postedMessage
	}

	if len(embeddableGameserver) == 0 {
		return
	}

	if lastMessage == nil {
		return
	}

	prErr := ch.PostReaction(lastMessage.ChannelID, lastMessage.ID, ch.Command.Reactions["restart_gameserver"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	mErr := AddPotentialRestartGameserverToDB(ch.Config, lastMessage.ID, message.Author.ID, gameserver)

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sg *RestartGameserver) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```ID: %d```", sg.Gameserver.ID)

	field := &discordgo.MessageEmbedField{
		Name:   sg.Gameserver.Name,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *RestartGameserverError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to find gameserver by ID",
		Value:  "\u200B",
		Inline: false,
	}

	return field, nil
}

// HandleRestartGameserverReaction func
func (ch *CommandHandler) HandleRestartGameserverReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	sg, gErr := GetPotentialRestartGameserverFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if sg.GameserverID == 0 {
		ch.Log.Log("ERROR: No potential gameserver found", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Unable to find original request to restart gameserver", "Please try to restart the gameserver again. It should fix the issue.", "", "", reaction.ChannelID)
		return
	}

	if sg.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to restart gameserver", ch.Log.LogInformation)
		return
	}

	gameserver, gsErr := ch.GetGameserverByID(sg.GameserverID)

	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Gameserver does not exist for ID: %d", sg.GameserverID), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Gameserver ID does not exist", "This could happen if you have changed your gameservers since opening the restart gameserver ticket.", "", fmt.Sprintf("stop %d", sg.GameserverID), reaction.ChannelID)
		return
	}

	if gameserver.ID == 0 || !gameserver.Enabled {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Game Server: Failed to get game server (%d) for guild (%s)", sg.GameserverID, ch.Guild.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Game Server", fmt.Sprintf("The game server (%d) does not exist or is disabled.", sg.GameserverID), "", "", "", reaction.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Command:    command,
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.RestartGameserver, requestData, nil)

	var embeddableFields []EmbeddableField
	var embeddableErrors []EmbeddableError

	if responses != nil && len(responses) > 0 {
		embeddableFields = append(embeddableFields, &RestartGameserver{
			Gameserver: responses[0].Gameserver,
		})
	}

	if errors != nil && len(errors) > 0 {
		embeddableErrors = append(embeddableErrors, &RestartGameserverError{
			Errs: []*utils.ServiceError{
				errors[0].Error,
			},
			Gameservers: []models.GameServer{
				gameserver,
			},
		})
	}

	embedParams := EmbeddableParams{
		Title:       "Restart Gameserver by ID",
		Description: fmt.Sprintf("**Restarted:** `%s`", gameserver.Name),
		Color:       ch.Config.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(reaction.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post restart gameserver message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	dErr := DeletePotentialRestartGameserverFromDB(ch.Config, sg)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// AddPotentialRestartGameserverToDB func
func AddPotentialRestartGameserverToDB(config *utils.Config, messageID string, userID string, gameserver models.GameServer) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	potentialRestart := &models.PotentialRestartGameserver{
		MessageID:      messageID,
		GameserverID:   gameserver.NitradoID,
		GameserverName: gameserver.Name,
		DiscordUserID:  userID,
	}

	mErr := potentialRestart.Create(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}

// GetPotentialRestartGameserverFromDB func
func GetPotentialRestartGameserverFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialRestartGameserver, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialRestartGameserver := &models.PotentialRestartGameserver{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbRes, mErr := potentialRestartGameserver.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbRes, nil
}

// DeletePotentialRestartGameserverFromDB func
func DeletePotentialRestartGameserverFromDB(config *utils.Config, sg *models.PotentialRestartGameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := sg.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
