package commands

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// StopGameserver struct
type StopGameserver struct {
	Gameserver models.GameServer
}

// StopGameserverError struct
type StopGameserverError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

const minLengthStopGameserverName = 7

// StopGameserverCommand func
func (ch *CommandHandler) StopGameserverCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Stop gameserver: %s", message.Content), ch.Log.LogInformation)

	if len(args) == 0 || len(args) > 1 {
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Invalid gameserver ID", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	gsID, cvErr := strconv.Atoi(args[0])
	if cvErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Stop Gameserver: Invalid game server ID: %s", cvErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Invalid game server ID", "", "", message.Content, message.ChannelID)
		return
	}

	gameserver, gsErr := ch.GetGameserverByID(gsID)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Stop Gameserver: %s: %d", gsErr.Error(), gsID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Game Server not found", "", "", message.Content, message.ChannelID)
	}

	if gameserver.ID == 0 || !gameserver.Enabled {
		ch.Log.Log(fmt.Sprintf("COMMAND: Stop Gameserver: Game Server not found: %d", gsID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Game Server not found", "", "", message.Content, message.ChannelID)
		return
	}

	var embeddableGameserver []EmbeddableField

	embeddableGameserver = append(embeddableGameserver, &StopGameserver{
		Gameserver: gameserver,
	})

	var embeddableErrors []EmbeddableError

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddableGameserver) > 0 {
		description += fmt.Sprintf("\nClick on the %s reaction to **stop** the gameserver", ch.Command.Reactions["stop_gameserver"].Icon)
	} else {
		description += "\n**No gameserver found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Stop Gameserver by ID",
		Description: description,
		Color:       ch.Config.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableGameserver, embeddableErrors)

	var lastMessage *discordgo.Message
	for _, embed := range embeds {
		// Can get message ID from response here
		postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post stop gameserver: %s", cpErr.Error()), ch.Log.LogMedium)
			continue
		}

		lastMessage = postedMessage
	}

	if len(embeddableGameserver) == 0 {
		return
	}

	if lastMessage == nil {
		return
	}

	prErr := ch.PostReaction(lastMessage.ChannelID, lastMessage.ID, ch.Command.Reactions["stop_gameserver"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	mErr := AddPotentialStopGameserverToDB(ch.Config, lastMessage.ID, message.Author.ID, gameserver)

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// GetGameserverByID func
func (ch *CommandHandler) GetGameserverByID(gameserverID int) (gameserver models.GameServer, se *utils.ServiceError) {
	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)

		return gameserver, &utils.ServiceError{
			StatusCode: http.StatusBadRequest,
			Err:        fmt.Errorf("Failed to start database"),
			Message:    "Failed to start database",
		}
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: ch.Guild.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)

		return gameserver, &utils.ServiceError{
			StatusCode: http.StatusBadRequest,
			Err:        fmt.Errorf("Failed to find game servers"),
			Message:    "Failed to find game servers",
		}
	}

	for _, aServer := range gameservers {
		if aServer.NitradoID == gameserverID {
			gameserver = aServer
			break
		}
	}

	if gameserver.ID != 0 {
		return gameserver, nil
	}

	return gameserver, &utils.ServiceError{
		StatusCode: http.StatusBadRequest,
		Err:        fmt.Errorf("No such gameserver in guild config"),
		Message:    fmt.Sprintf("Unable to find gameserver ID: %d", gameserverID),
	}
}

// ConvertToEmbedField func
func (sg *StopGameserver) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```ID: %d```", sg.Gameserver.ID)

	field := &discordgo.MessageEmbedField{
		Name:   sg.Gameserver.Name,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *StopGameserverError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to find gameserver by ID",
		Value:  "\u200B",
		Inline: false,
	}

	return field, nil
}

// HandleStopGameserverReaction func
func (ch *CommandHandler) HandleStopGameserverReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	sg, gErr := GetPotentialStopGameserverFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if sg.GameserverID == 0 {
		ch.Log.Log("ERROR: No potential gameserver found", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Unable to find original request to stop gameserver", "Please try to stop the gameserver again. It should fix the issue.", "", "", reaction.ChannelID)
		return
	}

	if sg.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to stop gameserver", ch.Log.LogInformation)
		return
	}

	gameserver, gsErr := ch.GetGameserverByID(sg.GameserverID)

	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Gameserver does not exist for ID: %d", sg.GameserverID), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Gameserver ID does not exist", "This could happen if you have changed your gameservers since opening the stop gameserver ticket.", "", fmt.Sprintf("stop %d", sg.GameserverID), reaction.ChannelID)
		return
	}

	if gameserver.ID == 0 || !gameserver.Enabled {
		ch.Log.Log(fmt.Sprintf("COMMAND: Stop Gameserver: Game Server not found: %d", sg.GameserverID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Game Server not found", "", "", "", reaction.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Command:    command,
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.StopGameserver, requestData, nil)

	var embeddableFields []EmbeddableField
	var embeddableErrors []EmbeddableError

	if responses != nil && len(responses) > 0 {
		embeddableFields = append(embeddableFields, &StopGameserver{
			Gameserver: responses[0].Gameserver,
		})
	}

	if errors != nil && len(errors) > 0 {
		embeddableErrors = append(embeddableErrors, &StopGameserverError{
			Errs: []*utils.ServiceError{
				errors[0].Error,
			},
			Gameservers: []models.GameServer{
				gameserver,
			},
		})
	}

	embedParams := EmbeddableParams{
		Title:       "Stop Gameserver by ID",
		Description: fmt.Sprintf("**Stopped:** `%s`", gameserver.Name),
		Color:       ch.Config.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(reaction.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post stop gameserver message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	dErr := DeletePotentialStopGameserverFromDB(ch.Config, sg)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// AddPotentialStopGameserverToDB func
func AddPotentialStopGameserverToDB(config *utils.Config, messageID string, userID string, gameserver models.GameServer) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	potentialBan := &models.PotentialStopGameserver{
		MessageID:      messageID,
		GameserverID:   gameserver.NitradoID,
		GameserverName: gameserver.Name,
		DiscordUserID:  userID,
	}

	mErr := potentialBan.Create(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}

// GetPotentialStopGameserverFromDB func
func GetPotentialStopGameserverFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialStopGameserver, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialStopGameserver := &models.PotentialStopGameserver{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbRes, mErr := potentialStopGameserver.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbRes, nil
}

// DeletePotentialStopGameserverFromDB func
func DeletePotentialStopGameserverFromDB(config *utils.Config, sg *models.PotentialStopGameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := sg.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
