package commands

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// ListGameserversCommand func
func (ch *CommandHandler) ListGameserversCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: List gameservers: %s", message.Content), ch.Log.LogInformation)

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log("COMMAND: List Game Servers: Failed to start database", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: List Game Servers", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	gameServers, ggsErr := guild.GetGameServers(dbInt)
	if ggsErr != nil {
		ch.Log.Log("COMMAND: List Game Servers: Failed to find guild or game servers", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: List Game Servers", "Failed to find guild or game servers. Please run setup first before attempting this and try again.", "", "", message.Content, message.ChannelID)
		return
	}

	var als map[uint]models.AdminLog = make(map[uint]models.AdminLog)
	var cls map[uint]models.ChatLog = make(map[uint]models.ChatLog)
	var pls map[uint]models.OnlinePlayer = make(map[uint]models.OnlinePlayer)

	for _, gs := range gameServers {
		admin := models.AdminLog{
			GameServerID: gs.ID,
		}
		chat := models.ChatLog{
			GameServerID: gs.ID,
		}
		players := models.OnlinePlayer{
			GameServerID: gs.ID,
		}

		admin, _ = admin.GetLatest(dbInt)
		chat, _ = chat.GetLatest(dbInt)
		players, _ = players.GetLatest(dbInt)

		if admin.ID != 0 {
			als[gs.ID] = admin
		}

		if chat.ID != 0 {
			cls[gs.ID] = chat
		}

		if players.ID != 0 {
			pls[gs.ID] = players
		}
	}

	embeds := formatListGameserversEmbed(ch.Config, gameServers, als, cls, pls, command.Name, command.Color)

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	for _, embed := range embeds {
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post embed to discord for list gameservers: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}
}

func formatListGameserversEmbed(c *utils.Config, gameServers []models.GameServer, als map[uint]models.AdminLog, cls map[uint]models.ChatLog, pls map[uint]models.OnlinePlayer, command string, embedColor int) []discordgo.MessageEmbed {
	var embeds []discordgo.MessageEmbed

	embed := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Listed",
		},
		Color:       embedColor,
		Description: fmt.Sprintf("**Command:** `%s`", command),
		Fields:      []*discordgo.MessageEmbedField{},
		Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       "Server List",
		URL:         c.Bot.TitleURL,
	}

	if len(gameServers) == 0 {
		field := &discordgo.MessageEmbedField{
			Name:   "No Gameservers",
			Value:  "Please run setup to add your game servers to the bot.",
			Inline: false,
		}

		newEmbed := embed

		newEmbed.Fields = append(newEmbed.Fields, field)

		embeds = append(embeds, newEmbed)
	} else {

		sort.SliceStable(gameServers, func(i, j int) bool {
			return strings.ToLower(gameServers[i].Name) < strings.ToLower(gameServers[j].Name)
		})

		embedNum := 0
		embedFields := 1

		for _, gameserver := range gameServers {
			if !gameserver.Enabled {
				continue
			}

			if embeds == nil || len(embeds) == 0 {
				newEmbed := embed
				embeds = append(embeds, newEmbed)
			}

			if embedFields >= 21 {
				embedNum++
				embedFields = 1
				newEmbed := embed
				embeds = append(embeds, newEmbed)
			}

			value := fmt.Sprintf("ID: %d", gameserver.NitradoID)

			value += "\nAdmin Log: "
			if val, ok := als[gameserver.ID]; ok {
				if val.Enabled {
					value += fmt.Sprintf("<#%s>", val.ChannelID)
				}
			}

			value += "\nChat Log: "
			if val, ok := cls[gameserver.ID]; ok {
				if val.Enabled {
					value += fmt.Sprintf("<#%s>", val.ChannelID)
				}
			}

			value += "\nOnline Players: "
			if val, ok := pls[gameserver.ID]; ok {
				if val.Enabled {
					value += fmt.Sprintf("<#%s>", val.ChannelID)
				}
			}

			field := &discordgo.MessageEmbedField{
				Name:   gameserver.Name,
				Value:  value,
				Inline: false,
			}

			embeds[embedNum].Fields = append(embeds[embedNum].Fields, field)
			embedFields++
		}
	}

	return embeds
}
