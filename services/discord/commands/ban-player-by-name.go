package commands

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// BanPlayer struct
type BanPlayer struct {
	PlayerName string
	PlayerID   string
	Servers    []models.GameServer
}

// BanPlayerError struct
type BanPlayerError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

const minLengthBanPlayerName = 3

// BanPlayerByNameCommand func
func (ch *CommandHandler) BanPlayerByNameCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Ban player: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 1 {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthSearchPlayerName), "", message.Content, message.ChannelID)
		return
	}

	potentialGameserverID, pe := strconv.Atoi(args[0])
	gameServer := models.GameServer{
		GuildID:   ch.Guild.ID,
		NitradoID: potentialGameserverID,
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: BanPlayerByName: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Ban Player by Name", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	if pe == nil {
		var gsErr *utils.ModelError
		gameServer, gsErr = gameServer.GetByGameServerID(dbInt)
		if gsErr != nil {
			ch.Log.Log(fmt.Sprintf("COMMAND: BanPlayerByName: Failed to search database for game server: %s", dbErr.Error()), ch.Log.LogInformation)
			ch.SendErrorMessage("ERROR: Ban Player by Name", "Failed to search the database for game server. Please try the command again.", "", "", message.Content, message.ChannelID)
			return
		}
	}

	var playerName string

	if gameServer.ID != 0 {
		if len(args) < 2 {
			ch.SendErrorMessage("ERROR: Ban Player", "Missing player name", fmt.Sprintf("Must include player name with %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
			return
		}

		playerName = strings.Join(args[1:], " ")
	} else if len(args[0]) > 5 && pe == nil {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Config.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
		return
	} else {
		playerName = strings.Join(args, " ")
	}

	if len(playerName) < minLengthBanPlayerName {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "true",
	}

	if gameServer.ID != 0 {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Player:     nitrado.Player{Name: playerName},
			Gameserver: gameServer,
		})
	} else {
		gameServer := models.GameServer{
			GuildID: ch.Guild.ID,
		}

		gameServers, gssErr := gameServer.Get(dbInt)
		if gssErr != nil {
			ch.Log.Log(fmt.Sprintf("COMMAND: BanPlayerByName: Failed to search database for game servers: %s", dbErr.Error()), ch.Log.LogInformation)
			ch.SendErrorMessage("ERROR: Ban Player by Name", "Failed to search the database for game servers. Please try the command again.", "", "", message.Content, message.ChannelID)
			return
		}

		if len(gameServers) == 0 {
			ch.SendErrorMessage("ERROR: Ban Player by Name", "No game servers linked to your Discord. Please run setup before attempting this command.", "", "", message.Content, message.ChannelID)
			return
		}

		for _, gameserver := range gameServers {
			if !gameserver.Enabled {
				continue
			}

			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{Name: playerName},
				Gameserver: gameserver,
			})
		}
	}

	responses, errors := request.GoRequest(request.SearchPlayersByName, requestData, params)

	// if len(responses) == 0 {
	// 	ch.SendErrorMessage("Ban Player", "No players found", "", "", message.Content, message.ChannelID)
	// 	return
	// }

	sp := ParsePlayersFromRequestResponse(responses)

	var embeddablePlayers []EmbeddableField

	for _, player := range sp {
		embeddablePlayers = append(embeddablePlayers, player)
	}

	var spe SearchPlayerError

	for _, err := range errors {
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		spe.Errs = append(spe.Errs, err.Error)
		spe.Gameservers = append(spe.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(spe.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &spe)
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddablePlayers) > 0 {
		if gameServer.ID != 0 {
			description += fmt.Sprintf("\nClick on the %s reaction to **ban** the player on %s", ch.Command.Reactions["ban_player"].Icon, gameServer.Name)
		} else {
			description += fmt.Sprintf("\nClick on the %s reaction to **ban** the player on all servers", ch.Command.Reactions["ban_player"].Icon)
		}
	} else {
		description += "\n**No player found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Ban Player by Name",
		Description: description,
		Color:       ch.Config.Bot.Commands["ban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddablePlayers, embeddableErrors)

	var lastMessage *discordgo.Message
	for _, embed := range embeds {
		// Can get message ID from response here
		postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post ban player: %s", cpErr.Error()), ch.Log.LogMedium)
			continue
		}

		lastMessage = postedMessage
	}

	if len(embeddablePlayers) == 0 {
		return
	}

	if lastMessage == nil {
		return
	}

	prErr := ch.PostReaction(lastMessage.ChannelID, lastMessage.ID, ch.Command.Reactions["ban_player"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	var mErr *utils.ModelError

	if gameServer.ID != 0 {
		mErr = AddPotentialBanToDB(ch.Config, lastMessage.ID, message.Author.ID, sp, gameServer.NitradoID)
	} else {
		mErr = AddPotentialBanToDB(ch.Config, lastMessage.ID, message.Author.ID, sp, 0)
	}

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// HandleBanReaction func
func (ch *CommandHandler) HandleBanReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	ban, gErr := GetPotentialBanFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if ban.PlayerID == "" {
		ch.Log.Log("ERROR: No potential ban found", ch.Log.LogInformation)
		return
	}

	if ban.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to ban user", ch.Log.LogInformation)
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: BanPlayerByName: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		return
	}

	defer dbInt.GetDB().Close()

	gameServer := models.GameServer{
		GuildID: ch.Guild.ID,
	}

	gameServers, gssErr := gameServer.Get(dbInt)
	if gssErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: BanPlayerByName: Failed to search database for game servers: %s", dbErr.Error()), ch.Log.LogInformation)
		return
	}

	if len(gameServers) == 0 {
		return
	}

	var requestData []nitrado.RequestData

	for _, gameserver := range gameServers {
		if !gameserver.Enabled {
			continue
		}

		if ban.GameserverID == 0 {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: ban.PlayerID},
				Gameserver: gameserver,
			})
		} else if ban.GameserverID == gameserver.NitradoID {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: ban.PlayerID},
				Gameserver: gameserver,
			})
		}
	}

	finished := false
	start := 0
	max := 10
	var responses []*nitrado.RequestResponse
	var errors []*nitrado.RequestError
	for !finished {
		if start >= len(requestData) {
			finished = true
		} else if start+max > len(requestData) {
			resps, errs := request.GoRequest(request.BanPlayer, requestData[start:], nil)
			responses = append(responses, resps...)
			errors = append(errors, errs...)
			finished = true
		} else {
			resps, errs := request.GoRequest(request.BanPlayer, requestData[start:start+max], nil)
			responses = append(responses, resps...)
			errors = append(errors, errs...)
			start = start + max
		}
	}

	b := ParseGameserversFromBanRequestResponse(responses)

	var embeddableFields []EmbeddableField

	if b != nil {
		b.PlayerID = ban.PlayerID
		b.PlayerName = ban.PlayerName
		embeddableFields = append(embeddableFields, b)
	}

	var ube BanPlayerError

	for _, err := range errors {
		// Ignore 404 errors
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		ube.Errs = append(ube.Errs, err.Error)
		ube.Gameservers = append(ube.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ube.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ube)
	}

	embedParams := EmbeddableParams{
		Title:       "Ban Player by Name",
		Description: fmt.Sprintf("**Banned:** `%s`\nThis will be applied when Nitrado updates Ark with the new ban. It may take some time.", b.PlayerName),
		Color:       ch.Config.Bot.Commands["ban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(reaction.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post ban player message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	dErr := DeletePotentialBanFromDB(ch.Config, ban)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sp *BanPlayer) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```Player ID: %s\nServers: %d```", sp.PlayerID, len(sp.Servers))

	field := &discordgo.MessageEmbedField{
		Name:   sp.PlayerName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *BanPlayerError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	charCount := 0
	for _, gs := range spe.Gameservers {
		if charCount+len(gs.Name) >= MaxEmbedFieldCharCount {
			break
		}

		gameservers = append(gameservers, gs.Name)
		charCount += len(gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to ban player on gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}

// ParseGameserversFromBanRequestResponse func
func ParseGameserversFromBanRequestResponse(rr []*nitrado.RequestResponse) *BanPlayer {
	var ubp BanPlayer

	for _, gs := range rr {
		ubp.Servers = append(ubp.Servers, gs.Gameserver)
	}

	return &ubp
}

// AddPotentialBanToDB func
func AddPotentialBanToDB(config *utils.Config, messageID string, userID string, players map[string]*SearchPlayer, gameserverID int) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	for _, player := range players {
		potentialBan := &models.PotentialBan{
			MessageID:     messageID,
			PlayerID:      player.ID,
			PlayerName:    player.Name,
			DiscordUserID: userID,
			GameserverID:  gameserverID,
		}

		mErr := potentialBan.Create(dbInt)

		if mErr != nil {
			return mErr
		}
	}

	return nil
}

// GetPotentialBanFromDB func
func GetPotentialBanFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialBan, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialBan := &models.PotentialBan{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbBan, mErr := potentialBan.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbBan, nil
}

// DeletePotentialBanFromDB func
func DeletePotentialBanFromDB(config *utils.Config, ban *models.PotentialBan) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := ban.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
