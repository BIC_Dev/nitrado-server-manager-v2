package commands

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// RemoveRole struct
type RemoveRole struct {
	CommandName string
	Roles       []string
}

// RemoveRoleError struct
type RemoveRoleError struct {
	CommandName string
	Roles       []string
}

// RemoveRoleCommand func
func (ch *CommandHandler) RemoveRoleCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Remove Role: %s", message.Content), ch.Log.LogInformation)

	commandName := strings.ToLower(args[0])
	roles := message.MentionRoles

	disc := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	discRoles, grErr := disc.GetRoles(message.GuildID)
	if grErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get Guild (%s) roles: %s", message.GuildID, grErr.Error()), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Remove Role", "Failed to get Guild roles to verify Administrator access", "", "", message.Content, message.ChannelID)
		return
	}

	var hasAdmin bool = false
	for _, memberRole := range message.Member.Roles {
		if hasAdmin {
			break
		}
		for _, guildRole := range discRoles {
			if memberRole == guildRole.ID && (guildRole.Permissions&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator) {
				hasAdmin = true
				break
			}
		}
	}

	if !hasAdmin {
		ch.Log.Log(fmt.Sprintf("Guild (%s) member with non-admin role attempted to run removerole: (%s - %s)", message.GuildID, message.Author.Username, message.Author.ID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Remove Role", "You do not have a role with Administrator permissions", "", "", message.Content, message.ChannelID)
		return
	}

	if len(roles) == 0 {
		ch.Log.Log("COMMAND: Remove Roles: No roles provided", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Remove Role", "You did not provide any roles. Please check the help info and run this again.", "", "", message.Content, message.ChannelID)
		return
	}

	foundCommand := false
	for key := range ch.Config.Bot.Commands {
		if key == commandName {
			foundCommand = true
		}
	}

	if !foundCommand {
		ch.Log.Log("COMMAND: Remove Roles: No command found with name: "+commandName, ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Remove Role", fmt.Sprintf("Command (%s) not found. Please check help information to see available commands.", commandName), "", "", message.Content, message.ChannelID)
		return
	}

	successOutput := RemoveRole{
		CommandName: commandName,
	}

	errorOutput := RemoveRoleError{
		CommandName: commandName,
	}

	var foundRoles []*discordgo.Role
	for _, role := range roles {
		isFound := false
		for _, gRole := range discRoles {
			if role == gRole.ID {
				foundRoles = append(foundRoles, gRole)
				isFound = true
				break
			}
		}
		if !isFound {
			errorOutput.Roles = append(errorOutput.Roles, role)
		}
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log("COMMAND: Remove Roles: Failed to start database", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Remove Role", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	guild, gErr := guild.GetLatest(dbInt)
	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Remove Roles: Failed to get guild: %s", message.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Remove Role", "Failed to find guild set up for bot. Please run setup first before removing command roles.", "", "", message.Content, message.ChannelID)
		return
	}

	for _, role := range foundRoles {
		aRole := models.CommandRole{
			GuildID:       guild.ID,
			CommandName:   commandName,
			DiscordRoleID: role.ID,
		}

		commandRole, crErr := aRole.GetCommandByRoleID(dbInt)
		if crErr != nil {
			ch.Log.Log(fmt.Sprintf("COMMAND: Remove Roles: Failed to lookup role (%s) for guild: %s", role.Name, message.GuildID), ch.Log.LogInformation)
			errorOutput.Roles = append(errorOutput.Roles, role.Name)
			continue
		}

		if commandRole.ID == 0 {
			continue
		}

		rcErr := commandRole.Delete(dbInt)
		if rcErr != nil {
			ch.Log.Log(fmt.Sprintf("COMMAND: Remove Roles: Failed to remove role (%s) for guild: %s", role.Name, message.GuildID), ch.Log.LogInformation)
			errorOutput.Roles = append(errorOutput.Roles, role.Name)
			continue
		}

		successOutput.Roles = append(successOutput.Roles, role.Name)
	}
	embedParams := EmbeddableParams{
		Title:       "Remove Roles for Command",
		Description: fmt.Sprintf("**Removed Roles for Command:** `%s`", commandName),
		Color:       ch.Config.Bot.Commands["removerole"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	var embeddableFields []EmbeddableField
	embeddableFields = append(embeddableFields, &successOutput)

	var embeddableErrors []EmbeddableError
	embeddableErrors = append(embeddableErrors, &errorOutput)

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := disc.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post remove role message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}
}

// ConvertToEmbedField func
func (sp *RemoveRole) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var roles string

	for _, role := range sp.Roles {
		roles = roles + "\n" + role
	}

	fieldVal := fmt.Sprintf("```Roles Removed: %s```", roles)

	field := &discordgo.MessageEmbedField{
		Name:   "Removed Roles for " + sp.CommandName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *RemoveRoleError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var roles string

	for _, role := range spe.Roles {
		roles = roles + "\n" + role
	}

	fieldVal := fmt.Sprintf("```Roles Failed: %s```", roles)

	field := &discordgo.MessageEmbedField{
		Name:   "Failed to remove roles for " + spe.CommandName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}
