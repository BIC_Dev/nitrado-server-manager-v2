package commands

import (
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// SetupStart struct
type SetupStart struct {
	Message string
}

// SetupFinish struct
type SetupFinish struct {
	Message string
}

// SetupError struct
type SetupError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

// SetupCommand func
func (ch *CommandHandler) SetupCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Setup: %s", message.Content), ch.Log.LogInformation)

	/*
		PRE-CHECK:
		1. Verify user has administrator access -
		2. Check if setup already in progress (TTL 15min?) -
		3. Check bot permissions to make sure it has access to create channels/categories -

		SETUP:
		1. Insert guild into DB -
		2. Check if nitrado tokens exist -
			2A. IF no tokens, throw error -
		3. Set up game servers -
			3A. Get all services -
			3B. Parse services for game servers -
			3C. Store game servers in DB -
		4. Create channels
			4A. Create category for Nitrado Server Manager -
			4B. Give bot permissions to category ?
			4C. Create channel(s) for chat logs -
			4D. Create channel(s) for admin logs -
			4E. Create channel(s) for online players -
		5. Output "Next Steps"
	*/

	dbInt, dbErr := db.GetDB(ch.Config)
	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get database to setup guild: %s", dbErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to get database", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	setup, gsErr := GetSetup(dbInt, message)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get setup info from DB: %s", gsErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to initialize setup", "", "", message.Content, message.ChannelID)
		return
	}

	if setup.Expires > (time.Now().Unix()+900) && !setup.Failed && !setup.Finished {
		ch.Log.Log(fmt.Sprintf("Failed to start setup due to already running setup: %s", message.GuildID), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "A setup is already running. Please wait for it to finish.", "", "", message.Content, message.ChannelID)
		return
	}

	var disableChannelCreation bool = false

	if len(args) == 0 {
		ch.Log.Log(fmt.Sprintf("Missing required parameters for setup command: %s", message.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Setup", "Missing required parameters for setup command. Please see `n!help` for more information.", "", "", message.Content, message.ChannelID)
		return
	}

	switch strings.ToLower(args[0]) {
	case "nochannels":
		disableChannelCreation = true
	case "channels":
		disableChannelCreation = false
	default:
		ch.Log.Log(fmt.Sprintf("Invalid parameters for setup command: %s", message.GuildID), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Setup", "Invalid parameters for setup command. Please see `n!help` for more information.", "", "", message.Content, message.ChannelID)
		return
	}

	embedParams := EmbeddableParams{
		Title:       "Setup",
		Description: "**Setup In Progress**",
		Color:       ch.Config.Bot.Commands["setup"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	var embeddableFields []EmbeddableField
	embeddableFields = append(embeddableFields, &SetupStart{
		Message: "Setup has started.",
	})

	var embeddableErrors []EmbeddableError

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post setup start message: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	var ssErr error
	setup, ssErr = StartSetup(dbInt, message)
	if ssErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to save start setup info in DB: %s", ssErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to initialize setup", "", "", message.Content, message.ChannelID)
		return
	}

	token := models.Token{
		GuildID: message.GuildID,
	}

	tokens, tErr := token.Get(dbInt)
	if tErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get tokens for setup: %s", tErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to get Nitrado Token information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	if len(tokens) == 0 {
		ch.Log.Log(fmt.Sprintf("Attempting to setup without tokens: %s", message.GuildID), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Setup", "Please contact BIC to set up your Nitrado tokens first", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	dGuild, dgErr := ch.DG.Guild(message.GuildID)
	if dgErr != nil {
		ch.SendErrorMessage("ERROR: Setup", "Failed to get Guild information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	guild := models.Guild{
		GuildID:     message.GuildID,
		GuildName:   dGuild.Name,
		ContactID:   message.Author.ID,
		ContactName: message.Author.Username,
		Enabled:     true,
	}

	ugErr := guild.Update(dbInt)

	if ugErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to save guild info for setup: %s", ugErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to save Guild information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	g, gErr := guild.GetLatest(dbInt)
	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get guild info for setup: %s", gErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to get Guild information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	} else if g.ID == 0 {
		ch.Log.Log(fmt.Sprintf("No guild found for setup: %s", message.GuildID), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to get Guild information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	var alCat models.DiscordCategory
	var clCat models.DiscordCategory
	var opCat models.DiscordCategory

	var alCatErr *utils.ServiceError
	var clCatErr *utils.ServiceError
	var opCatErr *utils.ServiceError

	if !disableChannelCreation {
		_, mgCatErr := CreateDiscordCategory(ch.DG, g, "management", dbInt)
		if mgCatErr != nil {
			ch.Log.Log(fmt.Sprintf("%s: %s", mgCatErr.Message, mgCatErr.Error()), ch.Log.LogHigh)
			ch.SendErrorMessage("ERROR: Setup", mgCatErr.Message, mgCatErr.Error(), "", message.Content, message.ChannelID)
			FailedSetup(dbInt, setup)
			return
		}

		alCat, alCatErr = CreateDiscordCategory(ch.DG, g, "admin-logs", dbInt)
		if alCatErr != nil {
			ch.Log.Log(fmt.Sprintf("%s: %s", alCatErr.Message, alCatErr.Error()), ch.Log.LogHigh)
			ch.SendErrorMessage("ERROR: Setup", alCatErr.Message, alCatErr.Error(), "", message.Content, message.ChannelID)
			FailedSetup(dbInt, setup)
			return
		}

		clCat, clCatErr = CreateDiscordCategory(ch.DG, g, "chat-logs", dbInt)
		if clCatErr != nil {
			ch.Log.Log(fmt.Sprintf("%s: %s", clCatErr.Message, clCatErr.Error()), ch.Log.LogHigh)
			ch.SendErrorMessage("ERROR: Setup", clCatErr.Message, clCatErr.Error(), "", message.Content, message.ChannelID)
			FailedSetup(dbInt, setup)
			return
		}

		opCat, opCatErr = CreateDiscordCategory(ch.DG, g, "online-players", dbInt)
		if opCatErr != nil {
			ch.Log.Log(fmt.Sprintf("%s: %s", opCatErr.Message, opCatErr.Error()), ch.Log.LogHigh)
			ch.SendErrorMessage("ERROR: Setup", opCatErr.Message, opCatErr.Error(), "", message.Content, message.ChannelID)
			FailedSetup(dbInt, setup)
			return
		}
	}

	gs := models.GameServer{
		GuildID: g.ID,
	}

	gameServers, gsgetErr := gs.Get(dbInt)
	if gsgetErr != nil {
		ch.Log.Log(fmt.Sprintf("Failed to get game server info for setup: %s", gsgetErr.Error()), ch.Log.LogHigh)
		ch.SendErrorMessage("ERROR: Setup", "Failed to get Game Server information", "", "", message.Content, message.ChannelID)
		FailedSetup(dbInt, setup)
		return
	}

	var onlinePlayers map[int]models.OnlinePlayer = make(map[int]models.OnlinePlayer)
	var chatLogs map[int]models.ChatLog = make(map[int]models.ChatLog)
	var adminLogs map[int]models.AdminLog = make(map[int]models.AdminLog)

	for _, server := range gameServers {
		op := models.OnlinePlayer{
			GameServerID: server.ID,
		}
		cl := models.ChatLog{
			GameServerID: server.ID,
		}
		al := models.AdminLog{
			GameServerID: server.ID,
		}

		a, aErr := op.GetLatest(dbInt)
		b, bErr := cl.GetLatest(dbInt)
		c, cErr := al.GetLatest(dbInt)

		if aErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to get online players info for setup: %s", aErr.Error()), ch.Log.LogHigh)
		}

		if bErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to get chat log info for setup: %s", bErr.Error()), ch.Log.LogHigh)
		}

		if cErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to get admin log info for setup: %s", cErr.Error()), ch.Log.LogHigh)
		}

		if a.ID != 0 {
			onlinePlayers[server.NitradoID] = a
		}

		if b.ID != 0 {
			chatLogs[server.NitradoID] = b
		}

		if c.ID != 0 {
			adminLogs[server.NitradoID] = c
		}
	}

	totalAccounts := len(tokens)
	totalGameServers := 0

	for _, t := range tokens {
		request := nitrado.NitradoRequest{
			Config:                ch.Config,
			NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
		}

		requestData := []nitrado.RequestData{}
		requestData = append(requestData, nitrado.RequestData{
			AccountName: t.TokenName,
		})

		// GET ALL SERVICES
		responses, errors := request.GoRequest(request.GetServices, requestData, nil)

		if len(errors) != 0 {
			var errs string
			for _, err := range errors {
				errs = fmt.Sprintf("%s; ERROR %s", errs, err.Error.Error())
			}

			ch.Log.Log(fmt.Sprintf("Failed to get services for token (%s) with errors: %s", t.TokenName, errs), ch.Log.LogMedium)
			continue
		}

		if len(responses) == 0 {
			ch.Log.Log(fmt.Sprintf("No services found for token %s", t.TokenName), ch.Log.LogLow)
			continue
		}

		for _, resp := range responses {
			for _, service := range resp.Services {
				alreadyExists := false
				for _, existingGS := range gameServers {
					if service.ID == existingGS.NitradoID {
						alreadyExists = true
						break
					}
				}

				if alreadyExists {
					ch.Log.Log(fmt.Sprintf("Game server already in DB: %d", service.ID), ch.Log.LogInformation)
					continue
				}

				if service.Status != "active" {
					ch.Log.Log(fmt.Sprintf("Non active service found for token %s", t.TokenName), ch.Log.LogInformation)
					continue
				}

				if service.Type != "gameserver" {
					ch.Log.Log(fmt.Sprintf("Non gameserver service found for token %s", t.TokenName), ch.Log.LogInformation)
					continue
				}

				if service.Details.FolderShort != "arkxb" && service.Details.FolderShort != "arkps" {
					ch.Log.Log(fmt.Sprintf("Non ark service found for token %s", t.TokenName), ch.Log.LogInformation)
					continue
				}

				// SAVE GAME SERVERS TO DB
				aGS := models.GameServer{
					NitradoID: service.ID,
					GuildID:   g.ID,
					TokenName: t.TokenName,
					Name:      service.Details.Name,
					Enabled:   true,
				}

				aGSErr := aGS.Update(dbInt)
				if aGSErr != nil {
					ch.Log.Log(fmt.Sprintf("Failed to save game server info for setup: %s", gsErr.Error()), ch.Log.LogHigh)
					continue
				}

				if !disableChannelCreation {
					ccErr := CreateChannels(ch, dbInt, aGS, alCat, clCat, opCat, g.GuildID, onlinePlayers, chatLogs, adminLogs)
					if ccErr != nil {
						ch.Log.Log(fmt.Sprintf("Failed to create channels for game servers: %s", ccErr.Error()), ch.Log.LogMedium)
					}
				}

				totalGameServers++

				// FOR TESTING
				//break
			}
		}
	}

	finishEmbedParams := EmbeddableParams{
		Title:       "Setup",
		Description: "**Setup Finished**",
		Color:       ch.Config.Bot.Commands["setup"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	var finishEmbeddableFields []EmbeddableField
	finishEmbeddableFields = append(finishEmbeddableFields, &SetupFinish{
		Message: fmt.Sprintf("Setup added %d Game Servers across %d Nitrado accounts.", totalGameServers, totalAccounts),
	})

	var finishEmbeddableErrors []EmbeddableError

	finishEmbeds := CreateEmbed(finishEmbedParams, finishEmbeddableFields, finishEmbeddableErrors)

	for _, embed := range finishEmbeds {
		// Can get message ID from response here
		_, cpfErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpfErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post setup finish message: %s", cpfErr.Error()), ch.Log.LogMedium)
		}
	}

	FinishedSetup(dbInt, setup)

	ch.Log.Log("FINISHED SETUP!!!", ch.Log.LogInformation)

	return
}

// CreateChannels creates Discord channels for game servers
func CreateChannels(ch *CommandHandler, dbInt db.Interface, gs models.GameServer, alCat models.DiscordCategory, clCat models.DiscordCategory, opCat models.DiscordCategory, guildID string, onlinePlayers map[int]models.OnlinePlayer, chatLogs map[int]models.ChatLog, adminLogs map[int]models.AdminLog) error {
	// CREATE CHANNELS
	var permissions []*discordgo.PermissionOverwrite
	permissions = append(permissions, &discordgo.PermissionOverwrite{
		ID:   guildID,
		Type: "role",
		Deny: discordgo.PermissionViewChannel,
	})

	if _, ok := onlinePlayers[gs.NitradoID]; !ok {
		opChan, opChanErr := ch.DG.GuildChannelCreate(guildID, fmt.Sprintf("players-%d", gs.NitradoID), discordgo.ChannelTypeGuildText)
		if opChanErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to create online players channel for setup: %s", opChanErr.Error()), ch.Log.LogMedium)
		} else {
			opm := models.OnlinePlayer{
				GameServerID: gs.ID,
				ChannelID:    opChan.ID,
				Enabled:      true,
			}
			dbOpErr := opm.Create(dbInt)
			if dbOpErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to save online players channel for setup: %s", dbOpErr.Error()), ch.Log.LogHigh)
			}
			_, ceErr := ch.DG.ChannelEditComplex(opChan.ID, &discordgo.ChannelEdit{
				Name:                 opChan.Name,
				ParentID:             opCat.CategoryID,
				PermissionOverwrites: permissions,
			})

			if ceErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to move online players channel for setup: %s", ceErr.Error()), ch.Log.LogMedium)
			}
		}
	}

	if _, ok := chatLogs[gs.NitradoID]; !ok {
		opChan, opChanErr := ch.DG.GuildChannelCreate(guildID, fmt.Sprintf("chat-%d", gs.NitradoID), discordgo.ChannelTypeGuildText)
		if opChanErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to create chat logs channel for setup: %s", opChanErr.Error()), ch.Log.LogMedium)
		} else {
			opm := models.ChatLog{
				GameServerID: gs.ID,
				ChannelID:    opChan.ID,
				Enabled:      true,
			}
			dbOpErr := opm.Create(dbInt)
			if dbOpErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to save chat log channel for setup: %s", dbOpErr.Error()), ch.Log.LogHigh)
			}

			_, ceErr := ch.DG.ChannelEditComplex(opChan.ID, &discordgo.ChannelEdit{
				Name:                 opChan.Name,
				ParentID:             clCat.CategoryID,
				PermissionOverwrites: permissions,
			})

			if ceErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to move chat logs channel for setup: %s", ceErr.Error()), ch.Log.LogMedium)
			}
		}
	}

	if _, ok := adminLogs[gs.NitradoID]; !ok {
		opChan, opChanErr := ch.DG.GuildChannelCreate(guildID, fmt.Sprintf("admin-%d", gs.NitradoID), discordgo.ChannelTypeGuildText)
		if opChanErr != nil {
			ch.Log.Log(fmt.Sprintf("Failed to create admin logs channel for setup: %s", opChanErr.Error()), ch.Log.LogMedium)
		} else {
			opm := models.AdminLog{
				GameServerID: gs.ID,
				ChannelID:    opChan.ID,
				Enabled:      true,
			}
			dbOpErr := opm.Create(dbInt)
			if dbOpErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to save admin log channel for setup: %s", dbOpErr.Error()), ch.Log.LogHigh)
			}

			_, ceErr := ch.DG.ChannelEditComplex(opChan.ID, &discordgo.ChannelEdit{
				Name:                 opChan.Name,
				ParentID:             alCat.CategoryID,
				PermissionOverwrites: permissions,
			})

			if ceErr != nil {
				ch.Log.Log(fmt.Sprintf("Failed to move admin logs channel for setup: %s", ceErr.Error()), ch.Log.LogMedium)
			}
		}
	}

	return nil
}

// StartSetup func
func StartSetup(dbInt db.Interface, message *discordgo.MessageCreate) (models.Setup, error) {
	setup := models.Setup{
		GuildID:     message.GuildID,
		ContactID:   message.Author.ID,
		ContactName: message.Author.Username,
		Expires:     time.Now().Unix() + 900, // 15 minutes
	}

	glErr := setup.Create(dbInt)
	if glErr != nil {
		return setup, glErr
	}

	setup, glErr = setup.GetLatest(dbInt)

	return setup, nil
}

// GetSetup func
func GetSetup(dbInt db.Interface, message *discordgo.MessageCreate) (models.Setup, error) {
	setup := models.Setup{
		GuildID: message.GuildID,
	}

	latest, glErr := setup.GetLatest(dbInt)
	if glErr != nil {
		return models.Setup{}, glErr
	}

	return latest, nil
}

// FailedSetup func
func FailedSetup(dbInt db.Interface, setup models.Setup) error {
	setup.Failed = true

	glErr := setup.Update(dbInt)
	if glErr != nil {
		return glErr
	}

	return nil
}

// FinishedSetup func
func FinishedSetup(dbInt db.Interface, setup models.Setup) error {
	setup.Finished = true
	glErr := setup.Update(dbInt)
	if glErr != nil {
		return glErr
	}

	return nil
}

// ConvertToEmbedField func
func (s *SetupStart) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var field *discordgo.MessageEmbedField

	field = &discordgo.MessageEmbedField{
		Name:   s.Message,
		Value:  "Please wait until setup completes before running it again. This process should only take 1-5 minutes.",
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (s *SetupFinish) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var field *discordgo.MessageEmbedField

	field = &discordgo.MessageEmbedField{
		Name:   s.Message,
		Value:  "Setup has finished running. Please run `n!servers` to get a list of all your Game Servers. You can use this information to rename the newly created channels. Also, please run `n!help` to see more available commands.",
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (s *SetupError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range s.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to setup gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}

// CreateDiscordCategory func
func CreateDiscordCategory(dg *discordgo.Session, guild models.Guild, categoryType string, dbInt db.Interface) (models.DiscordCategory, *utils.ServiceError) {
	category := models.DiscordCategory{
		GuildID:      guild.ID,
		CategoryType: categoryType,
	}

	var categoryName string
	switch categoryType {
	case "management":
		categoryName = "Server Management"
	case "admin-logs":
		categoryName = "Admin Logs"
	case "chat-logs":
		categoryName = "Chat Logs"
	case "online-players":
		categoryName = "Online Players"
	default:
		return category, &utils.ServiceError{
			Err:     fmt.Errorf("Invalid category type: %s", categoryType),
			Message: "Unable to set up category",
		}
	}

	var dcErr *utils.ModelError

	category, dcErr = category.GetLatest(dbInt)
	if dcErr != nil {
		return category, &utils.ServiceError{
			Err:     dcErr,
			Message: "Failed to get Discord category information",
		}
	}

	if category.ID == 0 {
		channel, cErr := dg.GuildChannelCreate(guild.GuildID, categoryName, discordgo.ChannelTypeGuildCategory)
		if cErr != nil {
			return category, &utils.ServiceError{
				Err:     cErr,
				Message: "Failed to create category for channels: " + categoryType,
			}
		}

		category.CategoryID = channel.ID

		dcErr := category.Create(dbInt)
		if dcErr != nil {
			return category, &utils.ServiceError{
				Err:     dcErr,
				Message: "Failed to create category for channels: " + categoryType,
			}
		}

		var permissions []*discordgo.PermissionOverwrite
		permissions = append(permissions, &discordgo.PermissionOverwrite{
			ID:   guild.GuildID,
			Type: "role",
			Deny: discordgo.PermissionViewChannel,
		})

		dg.ChannelEditComplex(category.CategoryID, &discordgo.ChannelEdit{
			PermissionOverwrites: permissions,
		})

		if categoryType == "management" {
			bcChannel, bcErr := dg.GuildChannelCreate(guild.GuildID, "bot-commands", discordgo.ChannelTypeGuildText)
			if bcErr != nil {
				return category, &utils.ServiceError{
					Err:     bcErr,
					Message: "Failed to create channel for bot-commands",
				}
			}

			dg.ChannelEditComplex(bcChannel.ID, &discordgo.ChannelEdit{
				PermissionOverwrites: permissions,
				ParentID:             channel.ID,
			})
		}
	}

	return category, nil
}
