package commands

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// OutputChannel struct
type OutputChannel struct {
	Command     utils.Command
	Channel     discordgo.Channel
	ChannelType string
	GameServer  models.GameServer
}

// OutputChannelError struct
type OutputChannelError struct {
	Command utils.Command
	Error   error
	Message string
}

// OutputChannelCommand func
func (ch *CommandHandler) OutputChannelCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Output Channel: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 2 {
		ch.Log.Log("Output Channel: Missing required parameters", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Missing parameters for the command. Please check `n!help` for more info on how to use this command.", "", "", message.Content, message.ChannelID)
		return
	}

	gameServerID := args[0]
	channelType := strings.ToLower(args[1])
	channelID := ""

	if len(args) >= 3 {
		channelID = args[2]
	}

	gameServerIDInt, gscErr := strconv.Atoi(gameServerID)
	if gscErr != nil {
		ch.Log.Log("Output Channel: Invalid Game Server ID", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Invalid Game Server ID. Please check `n!help` for more info on how to use this command.", "", "", message.Content, message.ChannelID)
		return
	}

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Output Channel: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	var gsErr *utils.ModelError
	gs := models.GameServer{
		GuildID:   ch.Guild.ID,
		NitradoID: gameServerIDInt,
	}

	gs, gsErr = gs.GetByGameServerID(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Output Channel: Failed to find game server: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Failed to find Game Server by ID. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	if gs.ID == 0 {
		ch.Log.Log("COMMAND: Output Channel: Failed to find game server", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Failed to find Game Server by ID. Please try the command again or run `n!setup` to add new Game Servers to bot.", "", "", message.Content, message.ChannelID)
		return
	}

	var channel *discordgo.Channel
	var ccErr error

	switch channelType {
	case "admin":
		channel, ccErr = SetAdminChannel(ch, message, &dbInt, gs, channelID)
		if ccErr != nil {
			ch.Log.Log(fmt.Sprintf("Output Channel: Failed to create admin log channel: %s", ccErr.Error()), ch.Log.LogMedium)
			ch.SendErrorMessage("ERROR: Output", "Failed to create admin log channel. Please check `n!help` for more info on how to use this command.", ccErr.Error(), "", message.Content, message.ChannelID)
			return
		}
	case "chat":
		channel, ccErr = SetChatChannel(ch, message, &dbInt, gs, channelID)
		if ccErr != nil {
			ch.Log.Log(fmt.Sprintf("Output Channel: Failed to create chat log channel: %s", ccErr.Error()), ch.Log.LogMedium)
			ch.SendErrorMessage("ERROR: Output", "Failed to create chat log channel. Please check `n!help` for more info on how to use this command.", ccErr.Error(), "", message.Content, message.ChannelID)
			return
		}
	case "players":
		channel, ccErr = SetPlayersChannel(ch, message, &dbInt, gs, channelID)
		if ccErr != nil {
			ch.Log.Log(fmt.Sprintf("Output Channel: Failed to create players channel: %s", ccErr.Error()), ch.Log.LogMedium)
			ch.SendErrorMessage("ERROR: Output", "Failed to create players channel. Please check `n!help` for more info on how to use this command.", ccErr.Error(), "", message.Content, message.ChannelID)
			return
		}
	default:
		ch.Log.Log(fmt.Sprintf("Output Channel: Invalid output channel type: %s", channelType), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Output", "Invalid output channel type. Please check `n!help` for more info on how to use this command.", "", "", message.Content, message.ChannelID)
		return
	}

	successOutput := OutputChannel{
		Command:     command,
		Channel:     *channel,
		ChannelType: channelType,
		GameServer:  gs,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embedParams := EmbeddableParams{
		Title:       "Output",
		Description: fmt.Sprintf("**New Output**: <#%s>", channel.ID),
		Color:       ch.Config.Bot.Commands["output"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	var embeddableFields []EmbeddableField
	embeddableFields = append(embeddableFields, &successOutput)

	var embeddableErrors []EmbeddableError

	embeds := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	for _, embed := range embeds {
		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post output channel: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}
}

// SetAdminChannel func
func SetAdminChannel(ch *CommandHandler, message *discordgo.MessageCreate, dbInt *db.Interface, gameServer models.GameServer, channelID string) (*discordgo.Channel, error) {
	if channelID != "" {
		pc := models.AdminLog{
			ChannelID: channelID,
		}

		channels, channelsErr := pc.GetByChannelID(*dbInt)
		if channelsErr != nil {
			return nil, channelsErr
		}

		totalEnabledGameServers := 0

		for _, aChan := range channels {
			if aChan.Enabled {
				totalEnabledGameServers++
			}
		}

		if totalEnabledGameServers >= 30 {
			return nil, fmt.Errorf("You have reached the maximum number of Game Servers (30) that can be put in one channel for Admin Logs")
		}
	}

	oChannel := models.AdminLog{
		GameServerID: gameServer.ID,
		Enabled:      true,
	}

	existingChannel, oChanErr := oChannel.GetLatest(*dbInt)
	if oChanErr != nil {
		return nil, oChanErr
	}

	if existingChannel.ID != 0 {
		oChannel = existingChannel
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	var channel *discordgo.Channel
	var cErr *utils.ServiceError

	if channelID != "" {
		channel, cErr = discord.GetChannel(channelID)
		if cErr != nil {
			switch cErr.StatusCode {
			case 10003: // Unknown channel
				return nil, fmt.Errorf("channel does not exist")
			case 10005: // Missing access
				return nil, fmt.Errorf("bot does not have access to the channel")
			case 0: // Unauthorized
				return nil, fmt.Errorf("bot is not authorized for your Discord")
			default:
				return nil, fmt.Errorf("unknown error occurred when trying to look up channel")
			}
		}
	} else {
		permissions := []*discordgo.PermissionOverwrite{
			{
				ID:   ch.Guild.GuildID,
				Type: "role",
				Deny: discordgo.PermissionViewChannel,
			},
		}
		channel, cErr = discord.CreateChannel(ch.Guild.GuildID, fmt.Sprintf("admin-%d", gameServer.NitradoID), "", permissions)
		if cErr != nil {
			return nil, cErr
		}
	}

	oChannel.ChannelID = channel.ID
	oChannel.Enabled = true
	upErr := oChannel.Update(*dbInt)
	if upErr != nil {
		return nil, upErr
	}

	return channel, nil
}

// SetChatChannel func
func SetChatChannel(ch *CommandHandler, message *discordgo.MessageCreate, dbInt *db.Interface, gameServer models.GameServer, channelID string) (*discordgo.Channel, error) {
	if channelID != "" {
		pc := models.ChatLog{
			ChannelID: channelID,
		}

		channels, channelsErr := pc.GetByChannelID(*dbInt)
		if channelsErr != nil {
			return nil, channelsErr
		}

		totalEnabledGameServers := 0

		for _, aChan := range channels {
			if aChan.Enabled {
				totalEnabledGameServers++
			}
		}

		if totalEnabledGameServers >= 10 {
			return nil, fmt.Errorf("You have reached the maximum number of Game Servers (10) that can be put in one channel for Chat Logs")
		}
	}

	oChannel := models.ChatLog{
		GameServerID: gameServer.ID,
		Enabled:      true,
	}

	existingChannel, oChanErr := oChannel.GetLatest(*dbInt)
	if oChanErr != nil {
		return nil, oChanErr
	}

	if existingChannel.ID != 0 {
		oChannel = existingChannel
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	var channel *discordgo.Channel
	var cErr *utils.ServiceError

	if channelID != "" {
		channel, cErr = discord.GetChannel(channelID)
		if cErr != nil {
			switch cErr.StatusCode {
			case 10003: // Unknown channel
				return nil, fmt.Errorf("channel does not exist")
			case 10005: // Missing access
				return nil, fmt.Errorf("bot does not have access to the channel")
			case 0: // Unauthorized
				return nil, fmt.Errorf("bot is not authorized for your Discord")
			default:
				return nil, fmt.Errorf("unknown error occurred when trying to look up channel")
			}
		}
	} else {
		permissions := []*discordgo.PermissionOverwrite{
			{
				ID:   ch.Guild.GuildID,
				Type: "role",
				Deny: discordgo.PermissionViewChannel,
			},
		}
		channel, cErr = discord.CreateChannel(ch.Guild.GuildID, fmt.Sprintf("chat-%d", gameServer.NitradoID), "", permissions)
		if cErr != nil {
			return nil, cErr
		}
	}

	oChannel.ChannelID = channel.ID
	oChannel.Enabled = true
	upErr := oChannel.Update(*dbInt)
	if upErr != nil {
		return nil, upErr
	}

	return channel, nil
}

// SetPlayersChannel func
func SetPlayersChannel(ch *CommandHandler, message *discordgo.MessageCreate, dbInt *db.Interface, gameServer models.GameServer, channelID string) (*discordgo.Channel, error) {
	if channelID != "" {
		pc := models.OnlinePlayer{
			ChannelID: channelID,
		}

		channels, channelsErr := pc.GetByChannelID(*dbInt)
		if channelsErr != nil {
			return nil, channelsErr
		}

		totalEnabledGameServers := 0

		for _, aChan := range channels {
			if aChan.Enabled {
				totalEnabledGameServers++
			}
		}

		if totalEnabledGameServers >= 20 {
			return nil, fmt.Errorf("You have reached the maximum number of Game Servers (20) that can be put in one channel for Online Players")
		}
	}

	oChannel := models.OnlinePlayer{
		GameServerID: gameServer.ID,
		Enabled:      true,
	}

	existingChannel, oChanErr := oChannel.GetLatest(*dbInt)
	if oChanErr != nil {
		return nil, oChanErr
	}

	if existingChannel.ID != 0 {
		oChannel = existingChannel
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	var channel *discordgo.Channel
	var cErr *utils.ServiceError

	if channelID != "" {
		channel, cErr = discord.GetChannel(channelID)
		if cErr != nil {
			switch cErr.StatusCode {
			case 10003: // Unknown channel
				return nil, fmt.Errorf("channel does not exist")
			case 10005: // Missing access
				return nil, fmt.Errorf("bot does not have access to the channel")
			case 0: // Unauthorized
				return nil, fmt.Errorf("bot is not authorized for your Discord")
			default:
				return nil, fmt.Errorf("unknown error occurred when trying to look up channel")
			}
		}
	} else {
		permissions := []*discordgo.PermissionOverwrite{
			{
				ID:   ch.Guild.GuildID,
				Type: "role",
				Deny: discordgo.PermissionViewChannel,
			},
		}
		channel, cErr = discord.CreateChannel(ch.Guild.GuildID, fmt.Sprintf("players-%d", gameServer.NitradoID), "", permissions)
		if cErr != nil {
			return nil, cErr
		}
	}

	oChannel.ChannelID = channel.ID
	oChannel.Enabled = true
	upErr := oChannel.Update(*dbInt)
	if upErr != nil {
		return nil, upErr
	}

	return channel, nil
}

// ConvertToEmbedField func
func (sp *OutputChannel) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "Changed channel for: " + sp.ChannelType,
		Value:  fmt.Sprintf("```Game Server: %s\nGame Server ID: %d\nChannel Name: %s\nChannel ID: %s```", sp.GameServer.Name, sp.GameServer.NitradoID, sp.Channel.Name, sp.Channel.ID),
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *OutputChannelError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "Failed to Set Channel",
		Value:  fmt.Sprintf("```Message: %s\nError: %s```", spe.Message, spe.Error.Error()),
		Inline: false,
	}

	return field, nil
}
