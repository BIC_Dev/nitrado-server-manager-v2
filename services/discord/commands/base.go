package commands

import (
	"fmt"
	"net/http"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
)

// CommandHandler struct
type CommandHandler struct {
	Config                *utils.Config
	Log                   *utils.Log
	DG                    *discordgo.Session
	NitradoV2ServiceToken string
	Guild                 models.Guild
	Command               utils.Command
	Reaction              utils.Reaction
}

// CommandError struct
type CommandError struct {
	Title   string
	Error   string
	Message string
	Usage   string
}

// EmbeddableParams struct
type EmbeddableParams struct {
	Title       string
	Description string
	Color       int
	TitleURL    string
	Footer      string
}

// EmbeddableField interface
type EmbeddableField interface {
	ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError)
}

// EmbeddableError interface
type EmbeddableError interface {
	ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError)
}

// MaxEmbedFields const
const MaxEmbedFields = 22 // actually 25

// MaxEmbedCharCount const
const MaxEmbedCharCount = 5000 // actually 6000

// MaxEmbedFieldCharCount const
const MaxEmbedFieldCharCount = 800 // actually 1000

// FormatErrorEmbed func
func formatErrorEmbed(c *utils.Config, err CommandError, command string, embedColor int) (*discordgo.MessageEmbed, *utils.ServiceError) {
	embed := &discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Error",
		},
		Color:       embedColor,
		Description: fmt.Sprintf("**Command:** `%s`", command),
		Fields:      []*discordgo.MessageEmbedField{},
		Timestamp:   time.Now().Format(time.RFC3339),
		Title:       err.Title,
		URL:         c.Bot.TitleURL,
	}

	if err.Message == "" && err.Usage == "" {
		field := &discordgo.MessageEmbedField{
			Name:   err.Error,
			Value:  "\u200b",
			Inline: false,
		}

		embed.Fields = append(embed.Fields, field)
	} else {
		field := &discordgo.MessageEmbedField{
			Name:   err.Error,
			Value:  fmt.Sprintf("%s\n%s", err.Message, err.Usage),
			Inline: false,
		}

		embed.Fields = append(embed.Fields, field)
	}

	return embed, nil
}

// SendErrorMessage func
func (ch *CommandHandler) SendErrorMessage(title string, err string, message string, usage string, commandContent string, channelID string) {
	errOutput := CommandError{
		Title:   title,
		Error:   err,
		Message: message,
		Usage:   usage,
	}

	var color = ch.Config.Bot.ErrorColor

	if ch.Config.Bot.ErrorColor != 0 {
		color = ch.Config.Bot.ErrorColor
	}

	embed, eErr := formatErrorEmbed(ch.Config, errOutput, commandContent, color)

	if eErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to create error embed for search players: %s", eErr.Error()), ch.Log.LogMedium)
		return
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	_, pErr := discord.CreatePost(channelID, "", embed)

	if pErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post error message: %s", pErr.Error()), ch.Log.LogHigh)
		return
	}
	return
}

// CreateEmbed func
func CreateEmbed(embedParams EmbeddableParams, embedableFields []EmbeddableField, embedableErrors []EmbeddableError) []discordgo.MessageEmbed {
	var embeds []discordgo.MessageEmbed

	embed := &discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Executed",
		},
		Color:       embedParams.Color,
		Description: embedParams.Description,
		Fields:      []*discordgo.MessageEmbedField{},
		Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       embedParams.Title,
		URL:         embedParams.TitleURL,
	}

	if embedParams.Footer != "" {
		embed.Footer.Text = embedParams.Footer
	}

	embedCharCount := 0
	for i := 0; i < len(embedableFields); i++ {
		field, err := embedableFields[i].ConvertToEmbedField()

		if err != nil {
			continue
		}

		if len(field.Name)+len(field.Value)+embedCharCount >= MaxEmbedCharCount || len(embed.Fields) >= MaxEmbedFields {
			embeds = append(embeds, *embed)
			embed.Fields = []*discordgo.MessageEmbedField{}
			embedCharCount = 0
		}

		embedCharCount += len(field.Name) + len(field.Value)
		embed.Fields = append(embed.Fields, field)
	}

	for i := 0; i < len(embedableErrors); i++ {
		field, err := embedableErrors[i].ConvertToEmbedField()

		if err != nil {
			continue
		}

		if len(field.Name)+len(field.Value)+embedCharCount >= MaxEmbedCharCount || len(embed.Fields) >= MaxEmbedFields {
			embeds = append(embeds, *embed)
			embed.Fields = []*discordgo.MessageEmbedField{}
			embedCharCount = 0
		}

		embedCharCount += len(field.Name) + len(field.Value)
		embed.Fields = append(embed.Fields, field)
	}

	if len(embed.Fields) != 0 || len(embeds) == 0 {
		embeds = append(embeds, *embed)
	}

	return embeds
}

// PostReaction func
func (ch *CommandHandler) PostReaction(channelID string, messageID string, icon string) *utils.ServiceError {
	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	rErr := discord.CreateReaction(channelID, messageID, icon)

	if rErr != nil {
		sErr := utils.NewServiceError(rErr)
		sErr.SetMessage(fmt.Sprintf("Failed to post reaction for message ID (%s): %s", messageID, rErr.GetMessage()))
		sErr.SetStatus(http.StatusFailedDependency)

		return sErr
	}

	return nil
}
