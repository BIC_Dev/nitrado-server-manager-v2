package commands

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Banlist struct
type Banlist struct {
	Players   []nitrado.Player
	Gamserver models.GameServer
}

// BanlistError struct
type BanlistError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

// MaxBanlistEmbedLength limits size of embed to not hit 6000 character limit
const MaxBanlistEmbedLength = 8

// BanlistCommand func
func (ch *CommandHandler) BanlistCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: List Bans: %s", message.Content), ch.Log.LogInformation)

	dbInt, dbErr := db.GetDB(ch.Config)

	if dbErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Banlist: Failed to start database: %s", dbErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Banlist", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: message.GuildID,
	}

	gameservers, gsErr := guild.GetGameServers(dbInt)
	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("COMMAND: Banlist: Failed to get game servers: %s", gsErr.Error()), ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Banlist", "Failed to start the database. Please try the command again.", "", "", message.Content, message.ChannelID)
		return
	}

	var singleBanListGameserver models.GameServer

	if len(args) > 0 {
		potentialGameserverID, pe := strconv.Atoi(args[0])

		if pe == nil {
			for _, gameserver := range gameservers {
				if !gameserver.Enabled {
					continue
				}
				if gameserver.NitradoID == potentialGameserverID {
					singleBanListGameserver = gameserver
					break
				}
			}
		}

		if singleBanListGameserver.ID == 0 {
			ch.SendErrorMessage("ERROR: Banlist", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Config.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
			return
		}
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "false",
	}

	if singleBanListGameserver.ID != 0 {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Gameserver: singleBanListGameserver,
		})
	} else {
		for _, gameserver := range gameservers {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Gameserver: gameserver,
			})
		}
	}

	responses, errors := request.GoRequest(request.GetBanlist, requestData, params)

	var embeddableBanlist []EmbeddableField

	for _, banlist := range responses {
		playerLen := len(banlist.Players)
		sliceStart := 0
		embedFieldSize := 0

		for i := 0; i < playerLen; i++ {
			embedFieldSize += len(banlist.Players[i].Name)
			if embedFieldSize >= 500 || i+1 >= playerLen {
				embeddableBanlist = append(embeddableBanlist, &Banlist{
					Players:   banlist.Players[sliceStart : i+1],
					Gamserver: banlist.Gameserver,
				})
				sliceStart = i + 1
				embedFieldSize = 0
			}
		}
	}

	var ble BanlistError

	for _, err := range errors {
		ble.Errs = append(ble.Errs, err.Error)
		ble.Gameservers = append(ble.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ble.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ble)
	}

	var description string = fmt.Sprintf("**Command:** `%s`", message.Content)

	embedParams := EmbeddableParams{
		Title:       "Get Banlist",
		Description: description,
		Color:       ch.Config.Bot.Commands["player"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	if len(embeddableBanlist) == 0 {
		embeds := CreateEmbed(embedParams, embeddableBanlist, embeddableErrors)

		for _, embed := range embeds {
			// Can get message ID from response here
			_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

			if cpErr != nil {
				ch.Log.Log(fmt.Sprintf("ERROR: Failed to post empty banlist: %s", cpErr.Error()), ch.Log.LogMedium)
			}
		}

		return
	}

	banlistSliceLen := len(embeddableBanlist)
	banlistSliceChunks := float64(banlistSliceLen) / float64(MaxBanlistEmbedLength)

	for i := 0; float64(i) < banlistSliceChunks; i++ {
		start := i * MaxBanlistEmbedLength
		end := start + MaxBanlistEmbedLength

		if end > banlistSliceLen {
			end = banlistSliceLen
		}

		embeds := CreateEmbed(embedParams, embeddableBanlist[start:end], embeddableErrors)

		for _, embed := range embeds {
			// Can get message ID from response here
			_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

			if cpErr != nil {
				ch.Log.Log(fmt.Sprintf("ERROR: Failed to post banlist: %s", cpErr.Error()), ch.Log.LogMedium)
			}
		}
	}

	return
}

// ConvertToEmbedField func
func (bl *Banlist) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var fieldVal string

	for _, player := range bl.Players {
		if player.Name == "" {
			continue
		}

		if fieldVal == "" {
			fieldVal = player.Name
		} else {
			fieldVal = fmt.Sprintf("%s\n%s", fieldVal, player.Name)
		}
	}

	var field *discordgo.MessageEmbedField

	field = &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("Banned Players on %s", bl.Gamserver.Name),
		Value:  fmt.Sprintf("```\n%s\n```", fieldVal),
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (ble *BanlistError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range ble.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to retreive banlist from gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}
