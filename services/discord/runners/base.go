package runners

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Runner struct
type Runner struct {
	Config                *utils.Config
	Log                   *utils.Log
	DG                    *discordgo.Session
	NitradoV2ServiceToken string
}

// MaxCharsPerField const
const MaxCharsPerField = 800 // actually 1000

// MaxFieldsPerEmbed const
const MaxFieldsPerEmbed = 22 // actually 25

// StartBackgroundProcesses func
func (r *Runner) StartBackgroundProcesses() {
	go r.OnlinePlayersRunner()
	go r.LogsRunner()
}

// GetAllGuilds func
func GetAllGuilds(dbInt db.Interface) (guilds []models.Guild, err *utils.ServiceError) {
	guild := models.Guild{}

	var gErr *utils.ModelError
	guilds, gErr = guild.GetAll(dbInt)
	if gErr != nil {
		return guilds, &utils.ServiceError{
			Err:     gErr,
			Message: "Failed to get all guilds",
		}
	}

	return guilds, err
}

// GetGameservers func
func GetGameservers(dbInt db.Interface, guild models.Guild) (gameservers []models.GameServer, err *utils.ServiceError) {
	var gsErr *utils.ModelError
	gameservers, gsErr = guild.GetGameServers(dbInt)
	if gsErr != nil {
		return gameservers, &utils.ServiceError{
			Err:     gsErr,
			Message: "Failed to get game servers",
		}
	}

	return gameservers, err
}
