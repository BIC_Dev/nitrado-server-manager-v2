package runners

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gammazero/workerpool"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Players struct
type Players struct {
	Players    []nitrado.Player
	GuildID    int64
	Gameserver models.GameServer
}

// OnlinePlayers struct
type OnlinePlayers struct {
	Players    []nitrado.Player
	Gameserver models.GameServer
}

// OnlinePlayersError struct
type OnlinePlayersError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

// OnlinePlayersRunner func
func (r *Runner) OnlinePlayersRunner() {
	r.Log.Log("PROCESS: Online players runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.PlayersInterval) * time.Second)

	wp := workerpool.New(r.Config.Runners.PlayersWorkers)

	for range ticker.C {
		// If workers have not finished last batch, skip this run
		if wp.WaitingQueueSize() > 0 {
			continue
		}

		dbInt, dbErr := db.GetDB(r.Config)

		if dbErr != nil {
			r.Log.Log("RUNNER: PLAYERS: Failed to start DB", r.Log.LogHigh)
			continue
		}

		guilds, gErr := GetAllGuilds(dbInt)
		if gErr != nil {
			r.Log.Log(fmt.Sprintf("RUNNER: PLAYERS: Failed to get all guilds: %s", gErr.Error()), r.Log.LogInformation)
			continue
		}

		for _, guild := range guilds {
			r.Log.Log(fmt.Sprintf("PROCESS: Running online players for guild: %s", guild.GuildID), r.Log.LogInformation)
			gameservers, gsErr := GetGameservers(dbInt, guild)
			if gsErr != nil {
				r.Log.Log(fmt.Sprintf("RUNNER: PLAYERS: Failed to get game servers: %s", gsErr.Error()), r.Log.LogInformation)
				continue
			}

			for _, gameserver := range gameservers {
				if gameserver.Enabled {
					r.AddOnlinePlayersJobToWorkerPool(wp, guild, gameserver)
				}
			}
		}

		dbInt.GetDB().Close()
	}
}

// AddOnlinePlayersJobToWorkerPool func
func (r *Runner) AddOnlinePlayersJobToWorkerPool(wp *workerpool.WorkerPool, guild models.Guild, gameserver models.GameServer) {
	wp.Submit(func() {
		r.OnlinePlayersJob(guild, gameserver)
	})
}

// OnlinePlayersJob func
func (r *Runner) OnlinePlayersJob(guild models.Guild, gameserver models.GameServer) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting online players for gameserver: %d", gameserver.NitradoID), r.Log.LogInformation)

	dbInt, dbErr := db.GetDB(r.Config)

	if dbErr != nil {
		r.Log.Log(fmt.Sprintf("JOB: PLAYERS: Failed to start DB: %s", dbErr.Error()), r.Log.LogHigh)
		return
	}

	defer dbInt.GetDB().Close()

	var opErr *utils.ModelError
	op := models.OnlinePlayer{
		GameServerID: gameserver.ID,
	}

	op, opErr = op.GetLatest(dbInt)
	if opErr != nil {
		r.Log.Log(fmt.Sprintf("LOGS JOB: No online players channel info in DB: %s", opErr.Error()), r.Log.LogInformation)
		return
	}

	if !op.Enabled {
		r.Log.Log(fmt.Sprintf("LOGS JOB: Online players not enabled for game server: %d", gameserver.NitradoID), r.Log.LogInformation)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                r.Config,
		NitradoV2ServiceToken: r.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":        "true",
		"exact-match":   "false",
		"cache-control": "no-cache",
	}

	requestData = append(requestData, nitrado.RequestData{
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.GetAllPlayers, requestData, params)

	for _, err := range errors {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.Error.Message, err.Error.Error()), r.Log.LogLow)
	}

	var totalOnlinePlayers int = 0

	var embeddableOnlinePlayers []commands.EmbeddableField

	for _, resp := range responses {
		if resp.Players == nil {
			continue
		}

		var onlinePlayers []OnlinePlayers
		var count int = 0
		var playersLen int = len(resp.Players)

		for i := 0; i < playersLen; i++ {
			totalOnlinePlayers++
			if len(onlinePlayers) <= count {
				onlinePlayers = append(onlinePlayers, OnlinePlayers{})
			}

			onlinePlayers[count].Players = append(onlinePlayers[count].Players, resp.Players[i])

			if math.Mod(float64(i+1), 40.0) == 0 {
				embeddableOnlinePlayers = append(embeddableOnlinePlayers, &onlinePlayers[count])
				count++
			} else if i+1 >= playersLen {
				embeddableOnlinePlayers = append(embeddableOnlinePlayers, &onlinePlayers[count])
			}
		}
	}

	var ope OnlinePlayersError

	for _, err := range errors {
		// if err.Error.StatusCode == http.StatusBadRequest {
		// 	continue
		// }

		ope.Errs = append(ope.Errs, err.Error)
		ope.Gameservers = append(ope.Gameservers, err.Gameserver)
	}

	var embeddableErrors []commands.EmbeddableError

	if len(ope.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ope)
	}

	// if len(embeddableErrors) == 0 && len(errors) > 0 && len(embeddableOnlinePlayers) == 0 {
	// 	return // If there are 400 responses coming back, don't output failed response to Discord
	// }

	guildID, gidErr := strconv.Atoi(guild.GuildID)
	if gidErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to convert guild ID: %s", gidErr.Error()), r.Log.LogMedium)
	}

	channelID, cidErr := strconv.Atoi(op.ChannelID)
	if cidErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to convert channel ID: %s", gidErr.Error()), r.Log.LogMedium)
	}

	onlinePlayersMessage, opErr := getOnlinePlayersMessage(int64(guildID), int64(channelID), gameserver.NitradoID, dbInt)

	if opErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", opErr.GetMessage(), opErr.Error()), r.Log.LogMedium)
		return
	}

	embedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("Online Players: %s", gameserver.Name),
		Description: fmt.Sprintf("Online players updated every %d seconds\nPlayers Online: %d", r.Config.Runners.PlayersInterval, totalOnlinePlayers),
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Updated",
	}

	embeds := commands.CreateEmbed(embedParams, embeddableOnlinePlayers, embeddableErrors)

	if len(embeds) == 0 {
		return
	}

	if onlinePlayersMessage.ID == 0 {
		go r.CreateOnlinePlayersPost("", &embeds[0], onlinePlayersMessage.DiscordMessageID, guild.GuildID, op, gameserver)
	} else {
		go r.UpdateOnlinePlayersPost("", &embeds[0], onlinePlayersMessage.DiscordMessageID, guild.GuildID, op, gameserver)
	}

	return
}

// ConvertToEmbedField func
func (op *OnlinePlayers) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var fieldVal string = ""

	for _, player := range op.Players {
		fieldVal += "🟢 " + player.Name + "\n\n"
	}

	field := &discordgo.MessageEmbedField{
		Name:   "\u200b",
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (ope *OnlinePlayersError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range ope.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	var errs string

	for _, err := range ope.Errs {
		errs = fmt.Sprintf("%s\n%s\n%s", errs, err.Message, err.Error())
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed Online Players Lookup",
		Value:  errs,
		Inline: false,
	}

	return field, nil
}

func getOnlinePlayersMessage(guildID int64, channelID int64, gameserverID int, dbInt db.Interface) (*models.OnlinePlayersMessage, *utils.ModelError) {
	onlinePlayersMessage := models.OnlinePlayersMessage{
		DiscordGuild:   guildID,
		DiscordChannel: channelID,
		GameserverID:   gameserverID,
	}

	dbStatusMessage, dbErr := onlinePlayersMessage.GetLatest(dbInt)

	if dbErr != nil {
		return nil, dbErr
	}

	return &dbStatusMessage, nil
}

func saveOnlinePlayersMessage(guildID int64, channelID int64, gameserverID int, discordMessageID int64, config *utils.Config) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return &utils.ModelError{
			Message: "Failed to initialize database",
			Err:     dbErr,
		}
	}

	defer dbInt.GetDB().Close()

	onlinePlayersMessage := models.OnlinePlayersMessage{
		DiscordGuild:     guildID,
		DiscordChannel:   channelID,
		DiscordMessageID: discordMessageID,
		GameserverID:     gameserverID,
	}

	opdbErr := onlinePlayersMessage.Update(dbInt)

	return opdbErr
}

// CreateOnlinePlayersPost func
func (r *Runner) CreateOnlinePlayersPost(content string, embed *discordgo.MessageEmbed, prevDiscordMessageID int64, guildID string, op models.OnlinePlayer, gameserver models.GameServer) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	message, cpErr := discord.CreatePost(op.ChannelID, "", embed)

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post online players for gameserver (%d): %s", gameserver.NitradoID, cpErr.Error()), r.Log.LogLow)

		if strings.Contains(cpErr.Error(), "Unknown Channel") {
			dbInt, dbErr := db.GetDB(r.Config)

			if dbErr != nil {
				return
			}

			op.Enabled = false
			upErr := op.Update(dbInt)
			if upErr != nil {
				r.Log.Log(fmt.Sprintf("LOGS JOB: Failed to disable missing online players channel: %s", op.ChannelID), r.Log.LogMedium)
			}

			dbInt.GetDB().Close()
		}
		return
	}

	messageID, piErr := strconv.ParseInt(message.ID, 10, 64)

	if piErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to parse message ID into int64 (%s): %s", message.ID, piErr.Error()), r.Log.LogHigh)
		return
	}

	if messageID != prevDiscordMessageID {
		guildIDInt, gidErr := strconv.Atoi(guildID)
		channelIDInt, cidErr := strconv.Atoi(op.ChannelID)
		if gidErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to parse guild ID into int64 (%s): %s", guildID, gidErr.Error()), r.Log.LogMedium)
			return
		}
		if cidErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to parse channel ID into int64 (%s): %s", op.ChannelID, gidErr.Error()), r.Log.LogMedium)
			return
		}
		sErr := saveOnlinePlayersMessage(int64(guildIDInt), int64(channelIDInt), gameserver.NitradoID, messageID, r.Config)

		if sErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: %s for gameserver (%d): %s", sErr.GetMessage(), gameserver.ID, sErr.Error()), r.Log.LogHigh)
			return
		}
	}

	return
}

// UpdateOnlinePlayersPost func
func (r *Runner) UpdateOnlinePlayersPost(content string, embed *discordgo.MessageEmbed, prevDiscordMessageID int64, guildID string, op models.OnlinePlayer, gameserver models.GameServer) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	var message *discordgo.Message
	var cpErr *utils.ServiceError
	var upErr *utils.ServiceError

	prevMessageIDString := fmt.Sprintf("%d", prevDiscordMessageID)
	message, upErr = discord.UpdatePost(op.ChannelID, prevMessageIDString, "", embed)

	if upErr != nil {
		if upErr.GetStatus() == 10008 {
			r.Log.Log(fmt.Sprintf("PROCESS: Creating new status post for gameserver (%d)", gameserver.ID), r.Log.LogInformation)
			message, cpErr = discord.CreatePost(op.ChannelID, "", embed)
		} else if strings.Contains(upErr.Error(), "Unknown Channel") {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to update online players due to missing channel for gameserver (%d): %s", gameserver.ID, upErr.Error()), r.Log.LogLow)

			dbInt, dbErr := db.GetDB(r.Config)

			if dbErr != nil {
				return
			}

			op.Enabled = false
			upErr := op.Update(dbInt)
			if upErr != nil {
				r.Log.Log(fmt.Sprintf("LOGS JOB: Failed to disable missing online players channel: %s", op.ChannelID), r.Log.LogMedium)
			}

			dbInt.GetDB().Close()
			return
		} else {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to update online players for gameserver (%d): %s", gameserver.ID, upErr.Error()), r.Log.LogLow)
			return
		}
	}

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post online players for gameserver (%d): %s", gameserver.ID, cpErr.Error()), r.Log.LogLow)
		return
	}

	messageID, piErr := strconv.ParseInt(message.ID, 10, 64)

	if piErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to parse message ID into int64 (%s): %s", message.ID, piErr.Error()), r.Log.LogHigh)
		return
	}

	if messageID != prevDiscordMessageID {
		guildIDInt, gidErr := strconv.Atoi(guildID)
		channelIDInt, cidErr := strconv.Atoi(op.ChannelID)
		if gidErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to parse guild ID into int64 (%s): %s", guildID, gidErr.Error()), r.Log.LogMedium)
			return
		}
		if cidErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to parse channel ID into int64 (%s): %s", op.ChannelID, gidErr.Error()), r.Log.LogMedium)
			return
		}
		sErr := saveOnlinePlayersMessage(int64(guildIDInt), int64(channelIDInt), gameserver.NitradoID, messageID, r.Config)

		if sErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: %s for gameserver (%d): %s", sErr.GetMessage(), gameserver.ID, sErr.Error()), r.Log.LogHigh)
			return
		}
	}

	return
}
