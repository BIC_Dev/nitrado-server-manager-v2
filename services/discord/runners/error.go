package runners

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
)

// ErrorHandler func
func (r *Runner) ErrorHandler(e chan *utils.ServiceError) {
	for {
		err := <-e
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.GetMessage(), err.Error()), r.Log.LogLow)
	}
}
