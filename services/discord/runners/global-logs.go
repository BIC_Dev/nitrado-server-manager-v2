package runners

import (
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gammazero/workerpool"
	"github.com/jinzhu/copier"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// PlayerLog struct
type PlayerLog struct {
	Gamertag  string `json:"gamertag"`
	Name      string `json:"name"`
	Message   string `json:"message"`
	Timestamp int64  `json:"timestamp"`
}

// AdminLog struct
type AdminLog struct {
	Name      string `json:"name"`
	Command   string `json:"command"`
	Timestamp int64  `json:"timestamp"`
}

// LogsError struct
type LogsError struct {
	Errs        []*utils.ServiceError
	Gameservers []models.GameServer
}

// MaxPlayerLogEmbedFields int
const MaxPlayerLogEmbedFields int = 15

// MaxAdminLogEmbedFields int
const MaxAdminLogEmbedFields int = 10

// MaxLinesPerEmbedField int
const MaxLinesPerEmbedField int = 5

// LogsRunner func
func (r *Runner) LogsRunner() {
	r.Log.Log("PROCESS: Logs runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.LogsInterval) * time.Second)

	wp := workerpool.New(r.Config.Runners.LogsWorkers)

	for range ticker.C {
		// If workers have not finished last batch, skip this run
		if wp.WaitingQueueSize() > 0 {
			continue
		}

		dbInt, dbErr := db.GetDB(r.Config)

		if dbErr != nil {
			r.Log.Log("RUNNER: LOGS: Failed to start DB", r.Log.LogHigh)
			continue
		}

		guilds, gErr := GetAllGuilds(dbInt)
		if gErr != nil {
			r.Log.Log(fmt.Sprintf("RUNNER: LOGS: Failed to get all guilds: %s", gErr.Error()), r.Log.LogInformation)
			continue
		}

		for _, guild := range guilds {
			r.Log.Log(fmt.Sprintf("PROCESS: Running logs for guild: %s", guild.GuildID), r.Log.LogInformation)
			gameservers, gsErr := GetGameservers(dbInt, guild)
			if gsErr != nil {
				r.Log.Log(fmt.Sprintf("RUNNER: LOGS: Failed to get game servers: %s", gsErr.Error()), r.Log.LogInformation)
				continue
			}
			for _, gameserver := range gameservers {
				if gameserver.Enabled {
					r.AddLogsJobToWorkerPool(wp, guild, gameserver)
				}
			}
		}

		dbInt.GetDB().Close()
	}
}

// AddLogsJobToWorkerPool func
func (r *Runner) AddLogsJobToWorkerPool(wp *workerpool.WorkerPool, guild models.Guild, gameserver models.GameServer) {
	wp.Submit(func() {
		r.LogsJob(guild, gameserver)
	})
}

// LogsJob func
func (r *Runner) LogsJob(guild models.Guild, gameserver models.GameServer) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting logs for gameserver: %d", gameserver.NitradoID), r.Log.LogInformation)

	dbInt, dbErr := db.GetDB(r.Config)

	if dbErr != nil {
		r.Log.Log(fmt.Sprintf("JOB: LOGS: Failed to start DB: %s", dbErr.Error()), r.Log.LogHigh)
		return
	}

	defer dbInt.GetDB().Close()

	var clErr *utils.ModelError
	cl := models.ChatLog{
		GameServerID: gameserver.ID,
	}

	var alErr *utils.ModelError
	al := models.AdminLog{
		GameServerID: gameserver.ID,
	}

	cl, clErr = cl.GetLatest(dbInt)
	if clErr != nil {
		r.Log.Log(fmt.Sprintf("LOGS JOB: No chat log channel info in DB: %s", clErr.Error()), r.Log.LogInformation)
		return
	}

	al, alErr = al.GetLatest(dbInt)
	if alErr != nil {
		r.Log.Log(fmt.Sprintf("LOGS JOB: Failed to get admin log info from DB: %s", alErr.Error()), r.Log.LogInformation)
		return
	}

	if !al.Enabled && !cl.Enabled {
		r.Log.Log(fmt.Sprintf("LOGS JOB: Admin and chat logs not enabled for game server: %d", gameserver.NitradoID), r.Log.LogInformation)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                r.Config,
		NitradoV2ServiceToken: r.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.GetGlobalLogs, requestData, nil)

	for _, err := range errors {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.Error.Message, err.Error.Error()), r.Log.LogLow)
	}

	var embeddablePlayerLogs []commands.EmbeddableField
	var embeddableAdminLogs []commands.EmbeddableField

	for _, resp := range responses {
		if resp.PlayerLogs != nil {
			charCount := 0

			var prevPlayerLog *PlayerLog
			playerLogLen := len(resp.PlayerLogs)

			for i := 0; i < playerLogLen; i++ {
				if prevPlayerLog == nil {
					prevPlayerLog = &PlayerLog{
						Gamertag:  resp.PlayerLogs[i].Gamertag,
						Name:      resp.PlayerLogs[i].Name,
						Message:   resp.PlayerLogs[i].Message,
						Timestamp: resp.PlayerLogs[i].Timestamp,
					}
					charCount = len(prevPlayerLog.Gamertag) + len(prevPlayerLog.Message) + len(prevPlayerLog.Name)
				} else if resp.PlayerLogs[i].Gamertag == prevPlayerLog.Gamertag && charCount+len(resp.PlayerLogs[i].Message) < MaxCharsPerField {
					prevPlayerLog.Message += fmt.Sprintf("\n%s", resp.PlayerLogs[i].Message)
					charCount += len(resp.PlayerLogs[i].Message)
				} else {
					var newPlayerLog PlayerLog
					copier.Copy(&newPlayerLog, &prevPlayerLog)
					embeddablePlayerLogs = append(embeddablePlayerLogs, &newPlayerLog)

					prevPlayerLog = &PlayerLog{
						Gamertag:  resp.PlayerLogs[i].Gamertag,
						Name:      resp.PlayerLogs[i].Name,
						Message:   resp.PlayerLogs[i].Message,
						Timestamp: resp.PlayerLogs[i].Timestamp,
					}
					charCount = len(prevPlayerLog.Gamertag) + len(prevPlayerLog.Message) + len(prevPlayerLog.Name)
				}

				if i+1 >= playerLogLen {
					embeddablePlayerLogs = append(embeddablePlayerLogs, prevPlayerLog)
				}
			}
		}

		if resp.AdminLogs != nil {
			charCount := 0

			var prevAdminLog *AdminLog
			adminLogLen := len(resp.AdminLogs)

			for i := 0; i < adminLogLen; i++ {
				if prevAdminLog == nil {
					prevAdminLog = &AdminLog{
						Name:      resp.AdminLogs[i].Name,
						Command:   resp.AdminLogs[i].Command,
						Timestamp: resp.AdminLogs[i].Timestamp,
					}
					charCount = len(prevAdminLog.Command) + len(prevAdminLog.Name)
				} else if resp.AdminLogs[i].Name == prevAdminLog.Name && charCount+len(resp.AdminLogs[i].Command) < MaxCharsPerField {
					prevAdminLog.Command += fmt.Sprintf("\n%s", resp.AdminLogs[i].Command)
					charCount += len(resp.AdminLogs[i].Command)
				} else {
					var newAdminLog AdminLog
					copier.Copy(&newAdminLog, &prevAdminLog)

					embeddableAdminLogs = append(embeddableAdminLogs, &newAdminLog)

					prevAdminLog = &AdminLog{
						Name:      resp.AdminLogs[i].Name,
						Command:   resp.AdminLogs[i].Command,
						Timestamp: resp.AdminLogs[i].Timestamp,
					}
					charCount = len(prevAdminLog.Command) + len(prevAdminLog.Name)
				}

				if i+1 >= adminLogLen {
					embeddableAdminLogs = append(embeddableAdminLogs, prevAdminLog)
				}
			}
		}
	}

	if len(embeddablePlayerLogs) == 0 {
		r.Log.Log(fmt.Sprintf("INFO: No player logs for gameserver ID: %d", gameserver.NitradoID), r.Log.LogInformation)
	}

	if len(embeddableAdminLogs) == 0 {
		r.Log.Log(fmt.Sprintf("INFO: No admin logs for gameserver ID: %d", gameserver.NitradoID), r.Log.LogInformation)
	}

	var le LogsError

	for _, err := range errors {
		le.Errs = append(le.Errs, err.Error)
		le.Gameservers = append(le.Gameservers, err.Gameserver)
	}

	var embeddableErrors []commands.EmbeddableError

	if len(le.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &le)
	}

	playerLogEmbedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("%s", gameserver.Name),
		Description: "Global chat log",
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Retrieved",
	}

	adminLogEmbedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("%s", gameserver.Name),
		Description: "Admin command log",
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Retrieved",
	}

	chatLogChannelID := cl.ChannelID
	adminLogChannelID := al.ChannelID

	if cl.Enabled {
		if len(embeddablePlayerLogs) > 0 {
			embeds := commands.CreateEmbed(playerLogEmbedParams, embeddablePlayerLogs, embeddableErrors)

			for _, embed := range embeds {
				if len(embed.Fields) == 0 {
					continue
				}
				_, msgErr := r.CreateLogPost(chatLogChannelID, "", &embed, gameserver.NitradoID)

				if msgErr != nil {
					if strings.Contains(msgErr.Error(), "Unknown Channel") {
						cl.Enabled = false
						upErr := cl.Update(dbInt)
						if upErr != nil {
							r.Log.Log(fmt.Sprintf("LOGS JOB: Failed to disable missing chat log channel: %s", cl.ChannelID), r.Log.LogMedium)
						}
					}
				}
			}
		}
	} else {
		r.Log.Log(fmt.Sprintf("LOGS JOB: Chat logs not enabled for game server: %d", gameserver.NitradoID), r.Log.LogInformation)
	}

	if al.Enabled {
		if len(embeddableAdminLogs) > 0 {
			embeds := commands.CreateEmbed(adminLogEmbedParams, embeddableAdminLogs, embeddableErrors)

			for _, embed := range embeds {
				if len(embed.Fields) == 0 {
					continue
				}
				_, msgErr := r.CreateLogPost(adminLogChannelID, "", &embed, gameserver.NitradoID)

				if msgErr != nil {
					if strings.Contains(msgErr.Error(), "Unknown Channel") {
						al.Enabled = false
						upErr := al.Update(dbInt)
						if upErr != nil {
							r.Log.Log(fmt.Sprintf("LOGS JOB: Failed to disable missing admin log channel: %s", cl.ChannelID), r.Log.LogMedium)
						}
					}
				}
			}
		}
	} else {
		r.Log.Log(fmt.Sprintf("LOGS JOB: Admin logs not enabled for game server: %d", gameserver.NitradoID), r.Log.LogInformation)
	}
}

// ConvertToEmbedField func
func (pl *PlayerLog) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	t := time.Unix(pl.Timestamp, 0)
	t = t.UTC()
	hms := fmt.Sprintf("%02d:%02d:%02d UTC", t.Hour(), t.Minute(), t.Second())

	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s - %s (%s)", hms, pl.Name, pl.Gamertag),
		Value:  pl.Message,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (al *AdminLog) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	t := time.Unix(al.Timestamp, 0)
	t = t.UTC()
	hms := fmt.Sprintf("%02d:%02d:%02d UTC", t.Hour(), t.Minute(), t.Second())

	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s - %s", hms, al.Name),
		Value:  al.Command,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (gle *LogsError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range gle.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed Retreiving Logs",
		Value:  "\u200b",
		Inline: false,
	}

	return field, nil
}

// CreateLogPost func
func (r *Runner) CreateLogPost(channelID string, content string, embed *discordgo.MessageEmbed, gameserverID int) (*discordgo.Message, *utils.ServiceError) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	message, cpErr := discord.CreatePost(channelID, content, embed)

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post chat log for gameserver (%d): %s", gameserverID, cpErr.Error()), r.Log.LogLow)
		return nil, cpErr
	}

	return message, nil
}
