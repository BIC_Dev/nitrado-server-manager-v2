package handler

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
)

// DiscordHandler strict
type DiscordHandler struct {
	Config                *utils.Config
	Log                   *utils.Log
	DG                    *discordgo.Session
	NitradoV2ServiceToken string
}

// SetupHandlers func
func (dh *DiscordHandler) SetupHandlers() {
	dh.DG.AddHandler(dh.MessageCreate)
	dh.DG.AddHandler(dh.MessageReaction)
}

func getPrefix(message string, prefixLength int) string {
	if len(message) < prefixLength {
		return ""
	}

	return strings.ToLower(message[0:prefixLength])
}

// IsAdmin func
func IsAdmin(dg *discordgo.Session, log *utils.Log, guildID string, roles []string) (bool, *utils.ServiceError) {
	disc := discordapi.Discord{
		DG:  dg,
		Log: log,
	}

	discRoles, grErr := disc.GetRoles(guildID)
	if grErr != nil {
		log.Log(fmt.Sprintf("Failed to get Guild (%s) roles: %s", guildID, grErr.Error()), log.LogMedium)
		return false, &utils.ServiceError{
			Err:     grErr,
			Message: "Failed to get Guild roles to verify Administrator access",
		}
	}

	var hasAdmin bool = false
	for _, memberRole := range roles {
		if hasAdmin {
			break
		}
		for _, guildRole := range discRoles {
			if memberRole == guildRole.ID && (guildRole.Permissions&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator) {
				hasAdmin = true
				break
			}
		}
	}

	return hasAdmin, nil
}
