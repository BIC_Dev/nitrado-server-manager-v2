package handler

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// MessageCreate func
func (dh *DiscordHandler) MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.Author.Bot {
		return
	}

	if len(m.Content) > len(dh.Config.Bot.Prefix) {
		if strings.ToLower(m.Content[:len(dh.Config.Bot.Prefix)]) != dh.Config.Bot.Prefix {
			return
		}
	} else {
		return
	}

	dbInt, dbErr := db.GetDB(dh.Config)

	if dbErr != nil {
		dh.Log.Log("COMMAND: Failed to start database", dh.Log.LogInformation)
		return
	}

	defer dbInt.GetDB().Close()

	commandHandler := &commands.CommandHandler{
		Config:                dh.Config,
		Log:                   dh.Log,
		DG:                    dh.DG,
		NitradoV2ServiceToken: dh.NitradoV2ServiceToken,
	}

	withoutPrefix := m.Content[len(dh.Config.Bot.Prefix):]
	split := strings.Split(withoutPrefix, " ")
	commandName := strings.ToLower(split[0])

	var command utils.Command

	if val, ok := dh.Config.Bot.Commands[commandName]; ok {
		command = val
		commandHandler.Command = command
	} else {
		dh.Log.Log(fmt.Sprintf("ERROR: Command not found: %s", commandName), dh.Log.LogInformation)
		commandHandler.SendErrorMessage("ERROR: Command not found", fmt.Sprintf("Command not found: %s", commandName), "Please use the help command to find available commands", "", m.Content, m.ChannelID)
		return
	}

	guild := models.Guild{
		GuildID: m.GuildID,
	}

	switch commandName {
	case "help":
	case "setup":
		break
	default:
		var gErr *utils.ModelError
		guild, gErr = guild.GetLatest(dbInt)
		if gErr != nil || guild.ID == 0 {
			dh.Log.Log("COMMAND: Failed to find guild", dh.Log.LogInformation)
			commandHandler.SendErrorMessage("ERROR: Bot Not Set Up", "The bot has not been set up for your Discord.", "Please use `n!setup` first before running other commands.", "", m.Content, m.ChannelID)
			return
		}

		if !guild.Enabled {
			dh.Log.Log("COMMAND: Guild has been disabled", dh.Log.LogInformation)
			commandHandler.SendErrorMessage("ERROR: Bot Disabled", "The bot has been disabled for your Discord. If you believe this to be an error, please contact BIC to re-enable it.", "", "", m.Content, m.ChannelID)
			return
		}
	}

	if guild.ID == 0 {
		switch commandName {
		case "help":
		case "setup":
			break
		default:
			dh.Log.Log(fmt.Sprintf("COMMAND: No guild setup to run command: %s", commandName), dh.Log.LogInformation)
			commandHandler.SendErrorMessage("ERROR: Bot Not Set Up", "The bot has not been set up for your Discord.", "Please use `n!setup` first before running other commands.", "", m.Content, m.ChannelID)
			return
		}
	}

	if guild.ID != 0 {
		commandHandler.Guild = guild
	}

	var foundRole = false
	var crs []models.CommandRole

	if admin, _ := IsAdmin(dh.DG, dh.Log, m.GuildID, m.Member.Roles); admin {
		foundRole = true
	} else {
		var crsErr *utils.ModelError
		crs, crsErr = guild.GetCommandRoles(dbInt)
		if crsErr != nil {
			dh.Log.Log(fmt.Sprintf("COMMAND: Failed to find guild roles: %s", crsErr.Error()), dh.Log.LogInformation)
			commandHandler.SendErrorMessage("ERROR: Failed to find roles for command", "The bot has not been set up for your Discord.", "Please use `n!setup` first before running other commands.", "", m.Content, m.ChannelID)
			return
		}
	}

	for _, commandRole := range crs {
		for _, userRole := range m.Member.Roles {
			if commandRole.DiscordRoleID == userRole && commandRole.CommandName == commandName {
				foundRole = true
				break
			}
		}

		if foundRole {
			break
		}
	}

	if !foundRole {
		commandHandler.SendErrorMessage("ERROR: Insufficient permissions", fmt.Sprintf("Insufficient permissions for command: %s", commandName), "You are not authorized to use this command.", "", m.Content, m.ChannelID)
		return
	}

	args := split[1:]

	switch commandName {
	case "help":
		commandHandler.HelpCommand(command, m, args)
	case "player":
		commandHandler.ListPlayersByNameCommand(command, m, args)
	case "ban":
		commandHandler.BanPlayerByNameCommand(command, m, args)
	case "unban":
		commandHandler.UnbanPlayerByNameCommand(command, m, args)
	case "servers":
		commandHandler.ListGameserversCommand(command, m, args)
	case "stop":
		commandHandler.StopGameserverCommand(command, m, args)
	case "restart":
		commandHandler.RestartGameserverCommand(command, m, args)
	case "banlist":
		commandHandler.BanlistCommand(command, m, args)
	case "setup":
		commandHandler.SetupCommand(command, m, args)
	case "addrole":
		commandHandler.AddRoleCommand(command, m, args)
	case "removerole":
		commandHandler.RemoveRoleCommand(command, m, args)
	case "nameserver":
		commandHandler.RenameGameserverCommand(command, m, args)
	case "output":
		commandHandler.OutputChannelCommand(command, m, args)
	default:
		// TODO: Send error message to Discord
		dh.Log.Log(fmt.Sprintf("Command not implemented: %s\n", commandName), dh.Log.LogLow)
		commandHandler.HelpCommand(command, m, args)
	}

	return
}
