package handler

import (
	"fmt"
	"net/http"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// MessageReaction func
func (dh *DiscordHandler) MessageReaction(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	if m.UserID == s.State.User.ID {
		return
	}

	command, cErr := GetReactionCommand(dh.Config, m.Emoji.Name)

	if cErr != nil {
		return
	}

	dbInt, dbErr := db.GetDB(dh.Config)

	if dbErr != nil {
		dh.Log.Log("COMMAND: Failed to start database", dh.Log.LogInformation)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: m.GuildID,
	}

	guild, gErr := guild.GetLatest(dbInt)
	if gErr != nil || guild.ID == 0 {
		dh.Log.Log("COMMAND: Failed to find guild", dh.Log.LogInformation)
		return
	}

	commandHandler := &commands.CommandHandler{
		Config:                dh.Config,
		Log:                   dh.Log,
		DG:                    dh.DG,
		NitradoV2ServiceToken: dh.NitradoV2ServiceToken,
		Guild:                 guild,
		Command:               command,
		Reaction:              command.Reactions[m.Emoji.ID],
	}

	if !guild.Enabled {
		dh.Log.Log("COMMAND: Guild has been disabled", dh.Log.LogInformation)
		commandHandler.SendErrorMessage("ERROR: Bot Disabled", "The bot has been disabled for your Discord. If you believe this to be an error, please contact BIC to re-enable it.", "", "", "", m.ChannelID)
		return
	}

	guildMember, gmErr := dh.DG.GuildMember(m.GuildID, m.UserID)

	if gmErr != nil {
		dh.Log.Log(fmt.Sprintf("ERROR: Could not look up guild member to validate roles: %s", gmErr.Error()), dh.Log.LogMedium)
		return
	}

	var foundRole = false

	var crs []models.CommandRole

	if admin, _ := IsAdmin(dh.DG, dh.Log, m.GuildID, guildMember.Roles); admin {
		foundRole = true
	} else {
		var crsErr *utils.ModelError
		crs, crsErr = guild.GetCommandRoles(dbInt)
		if crsErr != nil {
			dh.Log.Log(fmt.Sprintf("COMMAND: Failed to find guild roles: %s", crsErr.Error()), dh.Log.LogInformation)
			commandHandler.SendErrorMessage("ERROR: Failed to find roles for command", "The bot has not been set up for your Discord.", "Please use `n!setup` first before running other commands.", "", "", m.ChannelID)
			return
		}
	}

	for _, commandRole := range crs {
		for _, userRole := range guildMember.Roles {
			if commandRole.DiscordRoleID == userRole && commandRole.CommandName == command.Name {
				foundRole = true
				break
			}
		}

		if foundRole {
			break
		}
	}

	if !foundRole {
		dh.Log.Log(fmt.Sprintf("ERROR: Insufficient permissions to use reactions for: %s", command.Name), dh.Log.LogLow)
		return
	}

	switch command.Name {
	case "ban":
		commandHandler.HandleBanReaction(command, m)
	case "unban":
		commandHandler.HandleUnbanReaction(command, m)
	case "stop":
		commandHandler.HandleStopGameserverReaction(command, m)
	case "restart":
		commandHandler.HandleRestartGameserverReaction(command, m)
	default:
		dh.Log.Log(fmt.Sprintf("Reaction for command not implemented: %s\n", command.Name), dh.Log.LogInformation)
	}
}

// GetReactionCommand func
func GetReactionCommand(config *utils.Config, reactionName string) (utils.Command, *utils.ServiceError) {
	for _, aCommand := range config.Bot.Commands {
		for _, aReaction := range aCommand.Reactions {
			if reactionName == aReaction.Icon {
				return aCommand, nil
			}
		}
	}

	sErr := utils.NewServiceError(fmt.Errorf("No command associated with reaction"))
	sErr.SetMessage(fmt.Sprintf("No command associated with reaction name: %s", reactionName))
	sErr.SetStatus(http.StatusNotFound)

	return utils.Command{}, sErr
}
