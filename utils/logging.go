package utils

import (
	"encoding/json"
	"fmt"
	"log"
)

// Log struct
type Log struct {
	MinSeverity    int
	LogInformation int
	LogLow         int
	LogMedium      int
	LogHigh        int
	LogFatal       int
}

type logElement struct {
	Message  interface{}
	Severity string
}

// InitLog initializes the log with a minimum severity level
func InitLog(minSeverity int) *Log {
	log := Log{
		MinSeverity:    minSeverity,
		LogInformation: 1,
		LogLow:         2,
		LogMedium:      3,
		LogHigh:        4,
		LogFatal:       5,
	}

	return &log
}

// Log outputs the log
func (l *Log) Log(message interface{}, severity int) {
	if l.MinSeverity > severity {
		return
	}

	logElement := logElement{
		Message: message,
	}

	switch severity {
	case l.LogInformation:
		logElement.Severity = "INFORMATION"
	case l.LogLow:
		logElement.Severity = "LOW"
	case l.LogMedium:
		logElement.Severity = "MEDIUM"
	case l.LogHigh:
		logElement.Severity = "HIGH"
	case l.LogFatal:
		logElement.Severity = "FATAL"
	default:
		logElement.Severity = "UNKNOWN"
	}

	logJSON, err := json.Marshal(logElement)

	if err != nil {
		fmt.Println("Error creating json log: " + err.Error())
		return
	}

	if l.LogFatal == severity {
		log.Fatal(string(logJSON))
		return
	}

	fmt.Println(string(logJSON))
}
