package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Setup model
type Setup struct {
	gorm.Model
	GuildID     string `gorm:"varchar(50)" json:"guild_id"`
	ContactID   string `gorm:"varchar(50)" json:"contact_id"`
	ContactName string `gorm:"varchar(100)" json:"contact_name"`
	Expires     int64  `json:"expires"`
	Failed      bool   `json:"failed"`
	Finished    bool   `json:"finished"`
}

// TableName func
func (Setup) TableName() string {
	return "setups"
}

// Create adds a record to DB
func (t *Setup) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create setup entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GuildID
func (t *Setup) GetLatest(DBStruct db.Interface) (Setup, *utils.ModelError) {
	var setup Setup

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Last(&setup)

	if gorm.IsRecordNotFoundError(result.Error) {
		return setup, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find setup entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return Setup{}, modelError
	}

	return setup, nil
}

// Get gets the records for a Guild
func (t *Setup) Get(DBStruct db.Interface) ([]Setup, *utils.ModelError) {
	var setups []Setup

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Find(&setups)

	if gorm.IsRecordNotFoundError(result.Error) {
		return setups, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find setup entries in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return setups, modelError
	}

	return setups, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *Setup) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("id = ?", t.ID).Updates(map[string]interface{}{
		"contact_id":   t.ContactID,
		"contact_name": t.ContactName,
		"finished":     t.Finished,
		"failed":       t.Failed,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update setup entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
