package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

/*
id: 6206072 # [US][AFTM] Aftermath Cluster Crystal Isles 5xGather,20xTame,30xmat
account_name: "aftermath"
name: "Crystal Isles"
chat_log:
	enabled: false
	channel_id: 725862452302053528
admin_log:
	enabled: false
	channel_id: 734955648466550804
player_list:
	enabled: true
	channel_id: 734955648466550804
*/

// GameServer model
type GameServer struct {
	gorm.Model
	NitradoID int    `gorm:"index" json:"nitrado_id"`
	GuildID   uint   `gorm:"index" json:"guild_id"`
	TokenName string `gorm:"varchar(50)" json:"token_name"`
	Name      string `gorm:"varchar(100)" json:"name"`
	Enabled   bool   `json:"enabled"`
}

// TableName func
func (GameServer) TableName() string {
	return "game_servers"
}

// Create adds a record to DB
func (t *GameServer) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create game server entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// Get gets the records for a Guild
func (t *GameServer) Get(DBStruct db.Interface) ([]GameServer, *utils.ModelError) {
	var gameServers []GameServer

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Find(&gameServers)

	if gorm.IsRecordNotFoundError(result.Error) {
		return gameServers, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find game server entries in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return gameServers, modelError
	}

	return gameServers, nil
}

// GetByGameServerID gets the latest record for a GameServerID
func (t *GameServer) GetByGameServerID(DBStruct db.Interface) (GameServer, *utils.ModelError) {
	var commandRole GameServer

	result := DBStruct.GetDB().Where("guild_id = ? AND nitrado_id = ?", t.GuildID, t.NitradoID).Last(&commandRole)

	if gorm.IsRecordNotFoundError(result.Error) {
		return commandRole, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find game server entry by game server ID in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return GameServer{}, modelError
	}

	return commandRole, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *GameServer) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetByGameServerID(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("guild_id = ? AND nitrado_id = ?", t.GuildID, t.NitradoID).Updates(map[string]interface{}{
		"guild_id":   t.GuildID,
		"token_name": t.TokenName,
		"name":       t.Name,
		"enabled":    t.Enabled,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update game server entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
