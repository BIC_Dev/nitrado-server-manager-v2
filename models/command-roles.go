package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// CommandRole model
type CommandRole struct {
	gorm.Model
	GuildID         uint   `gorm:"index" json:"guild_id"`
	CommandName     string `gorm:"varchar(50)" json:"command_name"`
	DiscordRoleName string `gorm:"varchar(50)" json:"discord_role_name"`
	DiscordRoleID   string `gorm:"varchar(50)" json:"discord_role_id"`
}

// TableName func
func (CommandRole) TableName() string {
	return "command_roles"
}

// Create adds a record to DB
func (t *CommandRole) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create command role entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// Get gets the records for a Guild
func (t *CommandRole) Get(DBStruct db.Interface) ([]CommandRole, *utils.ModelError) {
	var commandRoles []CommandRole

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Find(&commandRoles)

	if gorm.IsRecordNotFoundError(result.Error) {
		return commandRoles, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find command role entries in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return commandRoles, modelError
	}

	return commandRoles, nil
}

// GetCommandByRoleID gets the latest record for a CommandRoleID
func (t *CommandRole) GetCommandByRoleID(DBStruct db.Interface) (CommandRole, *utils.ModelError) {
	var commandRole CommandRole

	result := DBStruct.GetDB().Where("guild_id = ? AND command_name = ? AND discord_role_id = ?", t.GuildID, t.CommandName, t.DiscordRoleID).Last(&commandRole)

	if gorm.IsRecordNotFoundError(result.Error) {
		return commandRole, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find command role entry by role ID in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return CommandRole{}, modelError
	}

	return commandRole, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *CommandRole) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetCommandByRoleID(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("guild_id = ? AND command_name = ? AND discord_role_id = ?", t.GuildID, t.CommandName, t.DiscordRoleID).Updates(map[string]interface{}{
		"guild_id":          t.GuildID,
		"command_name":      t.CommandName,
		"discord_role_name": t.DiscordRoleName,
		"discord_role_id":   t.DiscordRoleID,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update command role entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}

// Delete (soft) deletes a record from the database
func (t *CommandRole) Delete(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Delete(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to delete command role entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
