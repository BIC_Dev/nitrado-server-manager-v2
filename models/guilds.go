package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Guild model
type Guild struct {
	gorm.Model
	GuildID     string `gorm:"varchar(50) index" json:"guild_id"`
	GuildName   string `gorm:"varchar(200)" json:"guild_name"`
	ContactID   string `gorm:"varchar(50)" json:"contact_id"`
	ContactName string `gorm:"varchar(100)" json:"contact_name"`
	Enabled     bool   `json:"enabled"`
}

// TableName func
func (Guild) TableName() string {
	return "guilds"
}

// Create adds a record to DB
func (t *Guild) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create guild entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAll gets the latest record for a GuildID
func (t *Guild) GetAll(DBStruct db.Interface) ([]Guild, *utils.ModelError) {
	var guilds []Guild

	result := DBStruct.GetDB().Find(&guilds)

	if gorm.IsRecordNotFoundError(result.Error) {
		return guilds, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find guild entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return guilds, modelError
	}

	return guilds, nil
}

// GetLatest gets the latest record for a GuildID
func (t *Guild) GetLatest(DBStruct db.Interface) (Guild, *utils.ModelError) {
	var guild Guild

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Last(&guild)

	if gorm.IsRecordNotFoundError(result.Error) {
		return guild, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find guild entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return Guild{}, modelError
	}

	return guild, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *Guild) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("guild_id = ?", t.GuildID).Updates(map[string]interface{}{
		"guild_name":   t.GuildName,
		"contact_id":   t.ContactID,
		"contact_name": t.ContactName,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update guild entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}

// EnableGuild func
func (t *Guild) EnableGuild(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&t).Where("guild_id = ?", t.GuildID).Updates(map[string]interface{}{
		"enabled": true,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to enable guild in DB: %s", t.GuildID))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}

// DisableGuild func
func (t *Guild) DisableGuild(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&t).Where("guild_id = ?", t.GuildID).Updates(map[string]interface{}{
		"enabled": false,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to disable guild in DB: %s", t.GuildID))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}

// GetGameServers func
func (t *Guild) GetGameServers(DBStruct db.Interface) ([]GameServer, *utils.ModelError) {
	var gameServers []GameServer

	result := DBStruct.GetDB().Model(&t).Where("guilds.guild_id = ?", t.GuildID).Select("game_servers.*").Joins("INNER JOIN game_servers ON guilds.id = game_servers.guild_id").Scan(&gameServers)

	if gorm.IsRecordNotFoundError(result.Error) {
		return gameServers, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find guild entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return gameServers, modelError
	}

	return gameServers, nil
}

// GetCommandRoles func
func (t *Guild) GetCommandRoles(DBStruct db.Interface) ([]CommandRole, *utils.ModelError) {
	var commandRoles []CommandRole

	result := DBStruct.GetDB().Model(&t).Select("command_roles.*").Where("guilds.guild_id = ?", t.GuildID).Joins("JOIN command_roles ON guilds.id = command_roles.guild_id").Scan(&commandRoles)

	if gorm.IsRecordNotFoundError(result.Error) {
		return commandRoles, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find guild entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return commandRoles, modelError
	}

	return commandRoles, nil
}
