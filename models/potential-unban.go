package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// PotentialUnban struct
type PotentialUnban struct {
	MessageID     string `gorm:"varchar(50);index"`
	PlayerID      string `gorm:"varchar(50)"`
	PlayerName    string `gorm:"varchar(50)"`
	DiscordUserID string `gorm:"varchar(50)"`
	GameserverID  int
	gorm.Model
}

// TableName func
func (PotentialUnban) TableName() string {
	return "potential_unban"
}

// Create adds a record to DB
func (t *PotentialUnban) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create potential unban entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record
func (t *PotentialUnban) GetLatest(DBStruct db.Interface) (PotentialUnban, *utils.ModelError) {
	var potentialUnban PotentialUnban

	result := DBStruct.GetDB().Where("message_id = ? AND discord_user_id = ?", t.MessageID, t.DiscordUserID).Last(&potentialUnban)

	if gorm.IsRecordNotFoundError(result.Error) {
		return potentialUnban, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find potential unban by message_id and discord_user_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return PotentialUnban{}, modelError
	}

	return potentialUnban, nil
}

// Delete soft deletes a potential unban in DB
func (t *PotentialUnban) Delete(DBStruct db.Interface) *utils.ModelError {

	result := DBStruct.GetDB().Delete(&t)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to delete potential unban in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}
