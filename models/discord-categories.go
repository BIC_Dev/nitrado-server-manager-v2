package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// DiscordCategory model
type DiscordCategory struct {
	gorm.Model
	GuildID      uint   `gorm:"index" json:"guild_id"`
	CategoryID   string `gorm:"varchar(50)" json:"category_id"`
	CategoryType string `gorm:"varchar(20)" json:"category_type"`
}

// TableName func
func (DiscordCategory) TableName() string {
	return "discord_category"
}

// Create adds a record to DB
func (t *DiscordCategory) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create discord category entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GuildID
func (t *DiscordCategory) GetLatest(DBStruct db.Interface) (DiscordCategory, *utils.ModelError) {
	var dc DiscordCategory

	result := DBStruct.GetDB().Where("guild_id = ? AND category_type = ?", t.GuildID, t.CategoryType).Last(&dc)

	if gorm.IsRecordNotFoundError(result.Error) {
		return dc, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find discord category entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return DiscordCategory{}, modelError
	}

	return dc, nil
}

// Get gets the records for a Guild
func (t *DiscordCategory) Get(DBStruct db.Interface) ([]DiscordCategory, *utils.ModelError) {
	var discordCategories []DiscordCategory

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Find(&discordCategories)

	if gorm.IsRecordNotFoundError(result.Error) {
		return discordCategories, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find discord categories entries in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return discordCategories, modelError
	}

	return discordCategories, nil
}
