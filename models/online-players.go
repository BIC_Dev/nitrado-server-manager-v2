package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

/*
id: 6206072 # [US][AFTM] Aftermath Cluster Crystal Isles 5xGather,20xTame,30xmat
account_name: "aftermath"
name: "Crystal Isles"
chat_log:
	enabled: false
	channel_id: 725862452302053528
admin_log:
	enabled: false
	channel_id: 734955648466550804
player_list:
	enabled: true
	channel_id: 734955648466550804
*/

// OnlinePlayer model
type OnlinePlayer struct {
	gorm.Model
	GameServerID uint   `gorm:"index" json:"game_server_id"`
	ChannelID    string `gorm:"varchar(50)" json:"channel_id"`
	Enabled      bool   `json:"enabled"`
}

// TableName func
func (OnlinePlayer) TableName() string {
	return "online_players"
}

// Create adds a record to DB
func (t *OnlinePlayer) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create online players entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the records for a Guild
func (t *OnlinePlayer) GetLatest(DBStruct db.Interface) (OnlinePlayer, *utils.ModelError) {
	var onlinePlayer OnlinePlayer

	result := DBStruct.GetDB().Where("game_server_id = ?", t.GameServerID).Last(&onlinePlayer)

	if gorm.IsRecordNotFoundError(result.Error) {
		return onlinePlayer, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find online players entries in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return onlinePlayer, modelError
	}

	return onlinePlayer, nil
}

// GetByChannelID gets the records for a ChannelID
func (t *OnlinePlayer) GetByChannelID(DBStruct db.Interface) ([]OnlinePlayer, *utils.ModelError) {
	var onlinePlayers []OnlinePlayer

	result := DBStruct.GetDB().Where("channel_id = ?", t.ChannelID).Find(&onlinePlayers)

	if gorm.IsRecordNotFoundError(result.Error) {
		return onlinePlayers, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find online players channel entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return onlinePlayers, modelError
	}

	return onlinePlayers, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *OnlinePlayer) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("game_server_id = ?", t.GameServerID).Updates(map[string]interface{}{
		"channel_id": t.ChannelID,
		"enabled":    t.Enabled,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update online players entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
