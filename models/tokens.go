package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
)

// Token model
type Token struct {
	gorm.Model
	GuildID   string `gorm:"index" json:"guild_id"`
	TokenName string `gorm:"varchar(250)" json:"token_name"`
	Enabled   bool   `json:"enabled"`
}

// TableName func
func (Token) TableName() string {
	return "tokens"
}

// Create adds a record to DB
func (t *Token) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create token entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAll gets the records for a GuildID
func (t *Token) GetAll(DBStruct db.Interface) ([]Token, *utils.ModelError) {
	var tokens []Token

	result := DBStruct.GetDB().Find(&tokens)

	if gorm.IsRecordNotFoundError(result.Error) {
		return tokens, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find tokens entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return tokens, modelError
	}

	return tokens, nil
}

// Get gets the records for a GuildID
func (t *Token) Get(DBStruct db.Interface) ([]Token, *utils.ModelError) {
	var tokens []Token

	result := DBStruct.GetDB().Where("guild_id = ?", t.GuildID).Find(&tokens)

	if gorm.IsRecordNotFoundError(result.Error) {
		return tokens, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find token entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return tokens, modelError
	}

	return tokens, nil
}

// GetByTokenName gets the latest record for a token by name
func (t *Token) GetByTokenName(DBStruct db.Interface) (Token, *utils.ModelError) {
	var token Token

	result := DBStruct.GetDB().Where("guild_id = ? AND token_name = ?", t.GuildID, t.TokenName).Last(&token)

	if gorm.IsRecordNotFoundError(result.Error) {
		return token, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find token by name in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return token, modelError
	}

	return token, nil
}

// GetByID gets the latest record for a token by name
func (t *Token) GetByID(DBStruct db.Interface) (Token, *utils.ModelError) {
	var token Token

	result := DBStruct.GetDB().Where("id = ?", t.ID).Last(&token)

	if gorm.IsRecordNotFoundError(result.Error) {
		return token, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find token by ID in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return token, modelError
	}

	return token, nil
}

// EnableToken func
func (t *Token) EnableToken(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&t).Where("id = ?", t.ID).Updates(map[string]interface{}{
		"enabled": true,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to enable token in DB: %d", t.ID))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}

// DisableToken func
func (t *Token) DisableToken(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&t).Where("id = ?", t.ID).Updates(map[string]interface{}{
		"enabled": false,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to disable token in DB: %d", t.ID))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
