package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/controllers"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

type middleware struct {
	Log          *utils.Log
	ServiceToken string
}

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	middleware := middleware{
		Log:          c.Log,
		ServiceToken: c.ServiceToken,
	}

	router.HandleFunc("/nitrado-server-manager-v2/status", c.GetStatus).Methods("GET")

	router.HandleFunc("/nitrado-server-manager-v2/guild", c.GetAllGuilds).Methods("GET")
	router.HandleFunc("/nitrado-server-manager-v2/guild/{guild_id}", c.GetGuild).Methods("GET")

	router.HandleFunc("/nitrado-server-manager-v2/gameserver/{gameserver_id}", c.GetGameServer).Methods("GET").Queries("guild_id", "")
	router.HandleFunc("/nitrado-server-manager-v2/gameserver", c.EnableGameServer).Methods("PUT")
	router.HandleFunc("/nitrado-server-manager-v2/gameserver", c.DisableGameServer).Methods("DELETE")

	router.HandleFunc("/nitrado-server-manager-v2/token", c.GetTokens).Methods("GET")
	router.HandleFunc("/nitrado-server-manager-v2/token", c.AddToken).Methods("POST")
	router.HandleFunc("/nitrado-server-manager-v2/token", c.EnableToken).Methods("PUT")
	router.HandleFunc("/nitrado-server-manager-v2/token", c.DisableToken).Methods("DELETE")

	router.Use(middleware.loggingMiddleware)
	router.Use(middleware.authenticationMiddleware)
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	listenerPort := fmt.Sprintf(":%s", port)
	log.Fatal(http.ListenAndServe(listenerPort, router))
}

// Logs the request
func (m *middleware) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.RequestURI {
		case "/nitrado-server-manager-v2/status":
			next.ServeHTTP(w, r)
		default:
			m.Log.Log(fmt.Sprintf("ROUTE: Request to route %s", r.RequestURI), m.Log.LogInformation)
			next.ServeHTTP(w, r)
		}
	})
}

// Verifies the Service-Token header is set and authorized for access to the API
func (m *middleware) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serviceTokenHeader := r.Header.Get("Service-Token")

		switch r.RequestURI {
		case "/nitrado-server-manager-v2/status":
			next.ServeHTTP(w, r)
		default:
			if serviceTokenHeader != m.ServiceToken {
				m.Log.Log(fmt.Sprintf("ERROR: Invalid Service-Token header for route %s", r.RequestURI), m.Log.LogLow)
				controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
					Error:   "Invalid Service-Token header",
					Message: "An invalid Service-Token header was sent with request",
				}, http.StatusUnauthorized)
				return
			}

			next.ServeHTTP(w, r)
		}
	})
}
