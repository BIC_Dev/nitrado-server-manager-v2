package viewmodels

import "gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"

// GetTokensResponse struct
type GetTokensResponse struct {
	Message string         `json:"message"`
	Tokens  []models.Token `json:"tokens"`
}

// AddTokenRequest struct
type AddTokenRequest struct {
	GuildID   string `json:"guild_id"`
	TokenName string `json:"token_name"`
	Enabled   bool   `json:"enabled"`
}

// AddTokenResponse struct
type AddTokenResponse struct {
	Message string `json:"message"`
}

// EnableTokenRequest struct
type EnableTokenRequest struct {
	GuildID   string `json:"guild_id"`
	TokenName string `json:"token_name"`
}

// DisableTokenRequest struct
type DisableTokenRequest struct {
	GuildID   string `json:"guild_id"`
	TokenName string `json:"token_name"`
}

// EnableTokenResponse struct
type EnableTokenResponse struct {
	Message string `json:"message"`
}

// DisableTokenResponse struct
type DisableTokenResponse struct {
	Message string `json:"message"`
}
