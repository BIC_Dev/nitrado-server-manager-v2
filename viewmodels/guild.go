package viewmodels

import "gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"

// GetAllGuildsResponse struct
type GetAllGuildsResponse struct {
	Status string         `json:"status"`
	Guilds []models.Guild `json:"guilds"`
}

// GetGuildResponse struct
type GetGuildResponse struct {
	Guild             models.Guild             `json:"guild"`
	Tokens            []models.Token           `json:"tokens"`
	CommandRoles      []models.CommandRole     `json:"command_roles"`
	GameServers       []models.GameServer      `json:"game_servers"`
	ChatLogs          []models.ChatLog         `json:"chat_logs"`
	AdminLogs         []models.AdminLog        `json:"admin_logs"`
	OnlinePlayers     []models.OnlinePlayer    `json:"online_players"`
	DiscordCategories []models.DiscordCategory `json:"discord_categories"`
	Setups            []models.Setup           `json:"setups"`
}
