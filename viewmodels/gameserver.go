package viewmodels

import "gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"

// GetGameServerResponse struct
type GetGameServerResponse struct {
	Message    string            `json:"message"`
	GameServer models.GameServer `json:"game_server"`
}

// EnableGameServerRequest struct
type EnableGameServerRequest struct {
	GuildID      string `json:"guild_id"`
	GameServerID int    `json:"game_server_id"`
}

// EnableGameServerResponse struct
type EnableGameServerResponse struct {
	Message string `json:"message"`
}

// DisableGameServerRequest struct
type DisableGameServerRequest struct {
	GuildID      string `json:"guild_id"`
	GameServerID int    `json:"game_server_id"`
}

// DisableGameServerResponse struct
type DisableGameServerResponse struct {
	Message string `json:"message"`
}
