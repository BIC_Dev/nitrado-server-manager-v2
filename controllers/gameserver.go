package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

// GetGameServer responds with all tokens in DB
func (c *Controller) GetGameServer(w http.ResponseWriter, r *http.Request) {
	guildID := r.FormValue("guild_id")
	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]
	gsID, gsIDErr := strconv.Atoi(gameserverID)
	if gsIDErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gsIDErr.Error(),
			Message: "Failed to parse Game Server ID",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	var guildErr *utils.ModelError
	guild := models.Guild{
		GuildID: guildID,
	}

	guild, guildErr = guild.GetLatest(dbInt)
	if guildErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   guildErr.Error(),
			Message: guildErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if guild.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Guild exists with ID",
			Message: "No Guild exists with ID",
		}, http.StatusNotFound)
		return
	}

	var gsErr *utils.ModelError
	gs := models.GameServer{
		GuildID:   guild.ID,
		NitradoID: gsID,
	}
	gs, gsErr = gs.GetByGameServerID(dbInt)
	if gsErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gsErr.Error(),
			Message: gsErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if gs.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Game Server exists with ID",
			Message: "No Game Server exists with ID",
		}, http.StatusNotFound)
		return
	}

	response := viewmodels.GetGameServerResponse{
		Message:    "Found Game Server",
		GameServer: gs,
	}

	SendJSONResponse(w, response, http.StatusOK)
}

// EnableGameServer adds a token to the DB
func (c *Controller) EnableGameServer(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.EnableGameServerRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dcErr.Error(),
			Message: "Invalid JSON body for request",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	var guildErr *utils.ModelError
	guild := models.Guild{
		GuildID: body.GuildID,
	}

	guild, guildErr = guild.GetLatest(dbInt)
	if guildErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   guildErr.Error(),
			Message: guildErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if guild.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Guild exists with ID",
			Message: "No Guild exists with ID",
		}, http.StatusNotFound)
		return
	}

	var gsErr *utils.ModelError
	gs := models.GameServer{
		GuildID:   guild.ID,
		NitradoID: body.GameServerID,
	}
	gs, gsErr = gs.GetByGameServerID(dbInt)
	if gsErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gsErr.Error(),
			Message: gsErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if gs.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Game Server exists with ID",
			Message: "No Game Server exists with ID",
		}, http.StatusNotFound)
		return
	}

	gs.Enabled = true
	upErr := gs.Update(dbInt)
	if upErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   upErr.Error(),
			Message: upErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.EnableGameServerResponse{
		Message: "Successfully enabled Game Server",
	}, http.StatusOK)
}

// DisableGameServer adds a token to the DB
func (c *Controller) DisableGameServer(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.EnableGameServerRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dcErr.Error(),
			Message: "Invalid JSON body for request",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	var guildErr *utils.ModelError
	guild := models.Guild{
		GuildID: body.GuildID,
	}

	guild, guildErr = guild.GetLatest(dbInt)
	if guildErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   guildErr.Error(),
			Message: guildErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if guild.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Guild exists with ID",
			Message: "No Guild exists with ID",
		}, http.StatusNotFound)
		return
	}

	var gsErr *utils.ModelError
	gs := models.GameServer{
		GuildID:   guild.ID,
		NitradoID: body.GameServerID,
	}
	gs, gsErr = gs.GetByGameServerID(dbInt)
	if gsErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gsErr.Error(),
			Message: gsErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if gs.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No Game Server exists with ID",
			Message: "No Game Server exists with ID",
		}, http.StatusNotFound)
		return
	}

	gs.Enabled = false
	upErr := gs.Update(dbInt)
	if upErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   upErr.Error(),
			Message: upErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.DisableGameServerResponse{
		Message: "Successfully disabled Game Server",
	}, http.StatusOK)
}
