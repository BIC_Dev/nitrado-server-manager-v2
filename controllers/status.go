package controllers

import (
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

// GetStatus responds with the availability status of this service
func (c *Controller) GetStatus(w http.ResponseWriter, r *http.Request) {
	status := viewmodels.GetStatusResponse{
		Status:  "success",
		Message: "Service is available",
	}

	SendJSONResponse(w, status, http.StatusOK)
}
