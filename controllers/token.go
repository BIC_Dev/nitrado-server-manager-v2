package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

// GetTokens responds with all tokens in DB
func (c *Controller) GetTokens(w http.ResponseWriter, r *http.Request) {
	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	token := models.Token{}
	tokens, tErr := token.GetAll(dbInt)
	if tErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   tErr.Error(),
			Message: tErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	response := viewmodels.GetTokensResponse{
		Message: "Retrieved tokens for Guild",
		Tokens:  tokens,
	}

	SendJSONResponse(w, response, http.StatusOK)
}

// AddToken adds a token to the DB
func (c *Controller) AddToken(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.AddTokenRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dcErr.Error(),
			Message: "Invalid JSON body for request",
		}, http.StatusBadRequest)
		return
	}

	if body.GuildID == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "guild_id required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	if body.TokenName == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "token_name required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	token := models.Token{
		GuildID:   body.GuildID,
		TokenName: body.TokenName,
		Enabled:   body.Enabled,
	}

	existingToken, gbtnErr := token.GetByTokenName(dbInt)
	if gbtnErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gbtnErr.Error(),
			Message: gbtnErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if existingToken.ID != 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "token name already in use",
			Message: "Token already exists with name",
		}, http.StatusBadRequest)
		return
	}

	tErr := token.Create(dbInt)
	if tErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   tErr.Error(),
			Message: tErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.AddTokenResponse{
		Message: "Successfully added token",
	}, http.StatusOK)
}

// EnableToken adds a token to the DB
func (c *Controller) EnableToken(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.EnableTokenRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dcErr.Error(),
			Message: "Invalid JSON body for request",
		}, http.StatusBadRequest)
		return
	}

	if body.TokenName == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "token_name required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	if body.GuildID == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "guild_id required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	token := models.Token{
		GuildID:   body.GuildID,
		TokenName: body.TokenName,
	}

	existingToken, gbtnErr := token.GetByTokenName(dbInt)
	if gbtnErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gbtnErr.Error(),
			Message: gbtnErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if existingToken.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "no token found",
			Message: "Token does not exist by name",
		}, http.StatusBadRequest)
		return
	}

	etErr := existingToken.EnableToken(dbInt)
	if etErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   etErr.Error(),
			Message: etErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.EnableTokenResponse{
		Message: "Successfully enabled token",
	}, http.StatusOK)
}

// DisableToken adds a token to the DB
func (c *Controller) DisableToken(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.DisableTokenRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dcErr.Error(),
			Message: "Invalid JSON body for request",
		}, http.StatusBadRequest)
		return
	}

	if body.TokenName == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "token_name required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	if body.GuildID == "" {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "guild_id required",
			Message: "Missing parameter for request",
		}, http.StatusBadRequest)
		return
	}

	dbInt, dbErr := db.GetDB(c.Config)
	if dbErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: dbErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	token := models.Token{
		GuildID:   body.GuildID,
		TokenName: body.TokenName,
	}

	existingToken, gbtnErr := token.GetByTokenName(dbInt)
	if gbtnErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gbtnErr.Error(),
			Message: gbtnErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	if existingToken.ID == 0 {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "no token found",
			Message: "Token does not exist by name",
		}, http.StatusBadRequest)
		return
	}

	etErr := existingToken.DisableToken(dbInt)
	if etErr != nil {
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   etErr.Error(),
			Message: etErr.GetMessage(),
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.AddTokenResponse{
		Message: "Successfully disabled token",
	}, http.StatusOK)
}
