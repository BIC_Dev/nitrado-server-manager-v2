package controllers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/utils/db"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v2/viewmodels"
)

// GetAllGuilds responds with all currently loaded guilds
func (c *Controller) GetAllGuilds(w http.ResponseWriter, r *http.Request) {
	dbInt, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("Failed to start database: %s", dbErr.Error()), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: "Failed to start database",
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{}

	guilds, gErr := guild.GetAll(dbInt)
	if gErr != nil {
		c.Log.Log(fmt.Sprintf("Failed to get all guilds: %s", gErr.Error()), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gErr.Error(),
			Message: "Failed to get all guilds",
		}, http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.GetAllGuildsResponse{
		Status: "success",
		Guilds: guilds,
	}, http.StatusOK)
}

// GetGuild responds with all currently loaded guilds
func (c *Controller) GetGuild(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	guildID := vars["guild_id"]

	dbInt, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("Failed to start database: %s", dbErr.Error()), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   dbErr.Error(),
			Message: "Failed to start database",
		}, http.StatusInternalServerError)
		return
	}

	defer dbInt.GetDB().Close()

	guild := models.Guild{
		GuildID: guildID,
	}

	guild, gErr := guild.GetLatest(dbInt)
	if gErr != nil {
		c.Log.Log(fmt.Sprintf("Failed to find guild: %s", gErr.Error()), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   gErr.Error(),
			Message: "Failed to find guild",
		}, http.StatusInternalServerError)
		return
	}

	if guild.ID == 0 {
		c.Log.Log(fmt.Sprintf("Failed to find guild: %s", guildID), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			Error:   "No guild exists with ID",
			Message: "Failed to find guild",
		}, http.StatusNotFound)
		return
	}

	var tokens []models.Token = []models.Token{}
	var commandRoles []models.CommandRole = []models.CommandRole{}
	var gameServers []models.GameServer = []models.GameServer{}
	var chatLogs []models.ChatLog = []models.ChatLog{}
	var adminLogs []models.AdminLog = []models.AdminLog{}
	var onlinePlayers []models.OnlinePlayer = []models.OnlinePlayer{}
	var discordCategories []models.DiscordCategory = []models.DiscordCategory{}
	var setups []models.Setup = []models.Setup{}

	// var crErr *utils.ModelError
	// var gsErr *utils.ModelError
	// var clErr *utils.ModelError
	// var alErr *utils.ModelError
	// var opErr *utils.ModelError
	// var dcErr *utils.ModelError
	// var sErr *utils.ModelError

	t := models.Token{
		GuildID: guild.GuildID,
	}
	tokens, _ = t.Get(dbInt)

	cr := models.CommandRole{
		GuildID: guild.ID,
	}
	commandRoles, _ = cr.Get(dbInt)

	gs := models.GameServer{
		GuildID: guild.ID,
	}
	gameServers, _ = gs.Get(dbInt)

	for _, aGs := range gameServers {
		cl := models.ChatLog{
			GameServerID: aGs.ID,
		}

		aCl, aClErr := cl.GetLatest(dbInt)
		if aClErr != nil {
			continue
		}

		if aCl.ID != 0 {
			chatLogs = append(chatLogs, aCl)
		}
	}

	for _, aGs := range gameServers {
		al := models.AdminLog{
			GameServerID: aGs.ID,
		}

		aAl, aAlErr := al.GetLatest(dbInt)
		if aAlErr != nil {
			continue
		}

		if aAl.ID != 0 {
			adminLogs = append(adminLogs, aAl)
		}
	}

	for _, aGs := range gameServers {
		op := models.OnlinePlayer{
			GameServerID: aGs.ID,
		}

		aOp, aOpErr := op.GetLatest(dbInt)
		if aOpErr != nil {
			continue
		}

		if aOp.ID != 0 {
			onlinePlayers = append(onlinePlayers, aOp)
		}
	}

	dc := models.DiscordCategory{
		GuildID: guild.ID,
	}
	discordCategories, _ = dc.Get(dbInt)

	su := models.Setup{
		GuildID: guild.GuildID,
	}
	setups, _ = su.Get(dbInt)

	SendJSONResponse(w, viewmodels.GetGuildResponse{
		Guild:             guild,
		Tokens:            tokens,
		CommandRoles:      commandRoles,
		GameServers:       gameServers,
		ChatLogs:          chatLogs,
		AdminLogs:         adminLogs,
		OnlinePlayers:     onlinePlayers,
		DiscordCategories: discordCategories,
		Setups:            setups,
	}, http.StatusOK)
}
